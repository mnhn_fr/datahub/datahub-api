# API GRAPHQL DATAHUB MNHN

## Modèle de données

Le modèle données est une ontologie :

![L'ontologie pour le datahub](modele-pivot.png "L'ontologie pour le datahub")

[La documentation](https://datahub-models-mnhn-fr-datahub-8247b4bc9cc241b53c5db44d625a61dd.gitlab.io/)

## API GraphQL

### Structure

Dans cette api nous utilisons le type "Connection Cursor"

Ce type suit la spécification [Relay.dev](https://relay.dev/graphql/connections.htm)

Prenons le type `Person`. La transcription dans le schéma GraphQL est la suivante.

```
# Une connection GraphQL du type "Aptitude"
type PersonConnectionConnection {
    # Fenêtre de résultat au format AptitudeEdge
  edges: [PersonConnectionEdge]
  # Informations de pagination courante
  pageInfo: PageInfo!
}

# Un représentation d'une instance de Skill accompagnée de son curseur de pagination
type AptitudeEdge {
  # Le curseur de pagination
  cursor: String!
  # L'instance du type Aptitude
  node: Person
}

# Information de pagination 
type PageInfo {
  # Y a t-il des éléments suivants ? 
  hasNextPage: Boolean!
  # Quel est le curseur suivant (à passer au paramètre 'after' de la connection )
  endCursor: String
  # Y a t-il des éléments précédents ? 
  hasPreviousPage: Boolean!
  # Quel est le curseur précédent (à passer au paramètre 'after' de la connection )
  startCursor: String
}
 ```

### Filtrage et tri des résultats

Lors de la requête d'une Connection quelle qu'elle soit. Il est possible de filtrer et trier les résultats.

#### Paramètre qs

Le paramètre qs sert à chercher en mode Full-Text. Il concerne les propriétés scalaires textuelles.
Les formats suivants sont possibles :

- Recherche simple qs: "blabla"
- Recherche prefixée (pratique pour les autocomplete) qs: "^blabla.*"
- Recherche regex qs: "/blabla[^u]/"

#### Paramètre filters

Le paramètre filters est une liste de filtres au format ["[CLE] [OPERATOR] [VAL]", "[CLE] [OPERATOR] [VAL]", ...]. L'opérateur utilisé est un AND.


- [CLE] prend un nom de la propriété GraphQL sur laquel filtrer.
- [OPERATOR] prend une des valeurs =, !=, <, >, <=, >= dépendant du dataType de la propriété.
- [VAL] est la valeur de recherche. Elle peut prendre la forme :
  - D'un texte : ["scientificName = secamone falcata"]
  - D'un nombre : ["ratingValue >= 2", "ratingValue < 5"]
  - D'une URI quand il s'agit d'un lien : ["entityId  = https://www.ipni.org/n/968260-1"]
  - D'un chemin  ["rating.valueValue >= 2", "rating.value < 5"]

#### Paramètre sortings

Le paramètre sortings est une liste de tris au format [{ sortBy: "[CLE]", isSortDescending: true|false }].

- [CLE] prend un nom de la propriété GraphQL sur laquelle trier.

### Requêtes 

Exemple de requête sur le type person

```
query {
  person(first: 3, qs: "D. MANCHE") {
    edges {
      person: node {
        entityID
        name
      }
    }
  }
}
```

### Requête sur les Taxons

La requête sur le type "taxon" peut permettre de récupérer les alignements des taxons vers les référentiels externes tels que le GBIF, l'IPNI et TAXEF.

Cette requête répondra aux exigences suivantes :

- API Nom scientifique - Correspondances avec les référentiels externes (NS4)
- API Nom scientifique - Lien vers la page GBIF de l’espèce (NS2)

Il est possible de filtrer les résultats en fonction de la valeur du champ "inScheme".

Les valeurs possible de inScheme sont :

- https://www.gbif.org/species/
- https://www.ipni.org
- http://taxref.mnhn.fr/lod/taxref-ld

Un exemple de requête

````
query {
  taxon(filters: "taxonID = 1a9c3580-907e-51bd-b949-b7250c57c68d") {
    edges {
      taxon: node {
        entityId
        taxonID
        taxonAlignment {
          edges {
            taxonAlignment : node {
              entityId
              taxon {
                edges {
                  node {
                    entityId
                    scientificName
					inScheme
                  }
                }
              }
            }
          }
        }
      }
    }
  }
}

````

