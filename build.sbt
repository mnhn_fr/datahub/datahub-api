import Dependencies.*
import sbt.Keys.artifact

enablePlugins(sbtdocker.DockerPlugin, sbtassembly.AssemblyPlugin)
val meta = """META.INF(.)*""".r

ThisBuild / scalaVersion := "2.13.13"

lazy val root = (project in file("."))
  .settings(
    name := "datahub-api",
    description := "API GraphQL pour DATAHUB MNHN",
    libraryDependencies ++= Seq(
      scalaTest % Test,
      scalaLogging,
      logbackClassic,
      typesafeConfig,
      playJson,
      playJsonExt,
      rocksdb,
      synaptixEsToolkit,
      synaptixAPIToolkit,
      akkaHttpSprayJson,
      jansi,
      sangria,
      sangriaRelay,
      sangriaSpray
    ),
    assembly / test := {},
    assembly / assemblyMergeStrategy := {
      case PathList("javax", "servlet", xs@_*) => MergeStrategy.first
      case PathList(ps@_*) if ps.last endsWith ".html" => MergeStrategy.first
      //        case n if n.startsWith("application.conf") => MergeStrategy.concat
      case n if n.endsWith(".conf") => MergeStrategy.concat
      case n if n.endsWith(".properties") => MergeStrategy.concat
      case PathList("META-INF", "services", "org.apache.jena.system.JenaSubsystemLifecycle") => MergeStrategy.concat
      case PathList("META-INF", "services", "org.apache.spark.sql.sources.DataSourceRegister") => MergeStrategy.concat
      case PathList("META-INF", "services", xs@_*) => MergeStrategy.concat
      case PathList("META-INF", xs@_*) => MergeStrategy.discard
      case meta(_) => MergeStrategy.discard
      case x => MergeStrategy.first
    },
    Compile / mainClass := Some("fr.mnhn.datahub.api.DatahubGraphQLApiServerLauncher"),
    run / mainClass := Some("fr.mnhn.datahub.api.DatahubGraphQLApiServerLauncher"),
    assembly / mainClass := Some("fr.mnhn.datahub.api.DatahubGraphQLApiServerLauncher"),
    assembly /assemblyJarName  := "datahub-graphql-api.jar",
    docker / imageNames := Seq(
      ImageName(
        namespace = Some("registry.gitlab.com/mnhn_fr/datahub/datahub-api"),
        repository = artifact.value.name,
        tag = Some(version.value)
      )
    ),
    docker / buildOptions := BuildOptions(
      cache = false,
      platforms = List("linux/amd64")
    ),
    docker / dockerfile := {
      val artifact: File = assembly.value
      val artifactTargetPath = s"/app/${artifact.name}"

      new Dockerfile {
        from("openjdk:11-jre-slim-buster")
        add(artifact, artifactTargetPath)
        run("mkdir", "-p", "/data/")
        entryPoint("java", "-jar", artifactTargetPath)
      }
    }
  )