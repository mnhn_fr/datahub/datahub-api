import sbt._

object Version {
  lazy val synaptix = "3.1.5-SNAPSHOT"
  lazy val scalaVersion = "2.13.7"
  lazy val scalaTest = "3.2.9"
  lazy val scalaLogging = "3.9.5"
  lazy val sangria = "4.0.0"
  lazy val sangriaSpray = "1.0.3"
  lazy val scalaz = "7.3.6"

  lazy val logback = "1.3.5"
  lazy val tsConfig = "1.4.1"

  lazy val rocksdb = "8.5.3"
  lazy val playJson = "2.9.2"
  lazy val playJsonExt = "0.42.0"
}

object Dependencies {
  lazy val synaptixEsToolkit = "com.mnemotix" %% "synaptix-indexing-toolkit" % Version.synaptix
  lazy val synaptixAPIToolkit = "com.mnemotix" %% "synaptix-api-toolkit" % Version.synaptix

  lazy val scalaTest = "org.scalatest" %% "scalatest" % Version.scalaTest
  lazy val scalaLogging = "com.typesafe.scala-logging" %% "scala-logging" % Version.scalaLogging
  lazy val logbackClassic = "ch.qos.logback" % "logback-classic" % Version.logback
  lazy val typesafeConfig = "com.typesafe" % "config" % Version.tsConfig

  lazy val rocksdb = "org.rocksdb" % "rocksdbjni" % Version.rocksdb
  lazy val playJson = "com.typesafe.play" %% "play-json" % Version.playJson
  lazy val playJsonExt = "ai.x" %% "play-json-extensions" % Version.playJsonExt

  lazy val sangria = "org.sangria-graphql" %% "sangria" % Version.sangria
  lazy val sangriaRelay = "org.sangria-graphql" %% "sangria-relay" % Version.sangria
  lazy val sangriaSpray = "org.sangria-graphql" %% "sangria-spray-json" % Version.sangriaSpray

  lazy val akkaHttpSprayJson = "com.typesafe.akka" %% "akka-http-spray-json" % "10.2.7"
  lazy val jansi = "org.fusesource.jansi" % "jansi" % "2.3.2"
  lazy val scalaz = "org.scalaz" %% "scalaz-core" % Version.scalaz



}