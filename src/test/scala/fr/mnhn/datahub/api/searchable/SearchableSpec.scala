package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.model.GraphqlQueryArgument
import fr.mnhn.datahub.api.ApiSpec

import scala.concurrent.Await
import scala.concurrent.duration.DurationInt

class SearchableSpec extends ApiSpec {

  /**
   * we took the uri in the rdf data
   */

  it should "search in collection By entityID" in {
    val searchableCollection = new SearchableCollection()
    val future = searchableCollection.find(Seq("https://www.data.mnhn.fr/data/collection/2", "https://www.data.mnhn.fr/data/collection/22"))
    val collections = Await.result(future, 10.second)
    collections.size shouldBe(2)
  }

  it should "search in collection By graphQLArgument" in {
    val searchableCollection = new SearchableCollection()
    val graphQLQueryArgument = GraphqlQueryArgument(qs = Some("PAT"))
    val future = searchableCollection.search(Seq.empty, graphQLQueryArgument, None)
    val collections = Await.result(future, 10.second)
    collections.size should be > 0
    collections.size shouldBe 1
  }

  it should "search in collection-event by entityID" in {
    val searchableCollectionEvent = new SearchableCollectionEvent()
    val future = searchableCollectionEvent.find("https://www.data.mnhn.fr/data/collection-event/87d3a73b-51b9-5fa3-a48a-65df18f64506")
    val rs = Await.result(future, 10.second)
    println(rs)
    rs.isDefined shouldBe(true)
  }

  it should "search in collection-group by entityID" in {
    val searchableCollectionGroup = new SearchableCollectionGroup()
    val future = searchableCollectionGroup.find("https://www.data.mnhn.fr/data/collection-group/1")
    val rs = Await.result(future, 10.second)
    println(rs)
    rs.isDefined shouldBe(true)
  }

  it should "search in collection-group by graphQLArgument" in {
    val graphQLQueryArgument = GraphqlQueryArgument(qs = Some("botanique"))
    val searchableCollectionGroup = new SearchableCollectionGroup()
    val future = searchableCollectionGroup.search(Seq.empty, graphQLQueryArgument, None)
    val rs = Await.result(future, 10.second)
    println(rs)
    rs.size should be > 0
  }


    it should "search in concept entityID" in {
    val searchableConcept = new SearchableConcept()
    val future = searchableConcept.find("https://www.ipni.org/n/426625-1")
    val rs = Await.result(future, 10.second)
    println(rs)
    rs.isDefined shouldBe(true)
  }

  // todo strict ==
  it should "search in concept index a TaxonName by graphQLArgument" in {
    val graphQLQueryArgument = GraphqlQueryArgument(qs = Some("Gamolepis euriopoides"))
    val searchableTaxonName = new SearchableTaxonName()
    val future = searchableTaxonName.search(Seq.empty, graphQLQueryArgument, None)
    val rs = Await.result(future, 10.second)
    rs.foreach(r=> println(r.scientificName))
    println(rs.size)
    rs.size should be > 0
  }


  it should "search a geolocation by graphQLArgument" in {
    val graphQLQueryArgument = GraphqlQueryArgument(qs = Some("Brésil"))
    val searchableGeolocation = new SearchableGeolocation()
    val future = searchableGeolocation.search(Seq.empty, graphQLQueryArgument, None)
    val rs = Await.result(future, 10.second)
    rs.foreach(r=> println(r))
    println(rs.size)
    rs.size should be > 0
  }

  it should "search a geometry by entityID" in {
    val searchableGeometry = new SearchableGeometry()
    val future = searchableGeometry.find("https://www.data.mnhn.fr/data/geometry/55df4746-50a6-58a4-97fb-f36b827639ef")
    val rs = Await.result(future, 10.second)
    rs.foreach(r=> println(r))
    println(rs.size)
    rs.size should be > 0
  }

  it should "search an Identification by entityID" in {
    val searchableIdentification = new SearchableIdentification()
    val future = searchableIdentification.find("https://www.data.mnhn.fr/data/identification/197be09a-7afa-51f4-adab-f692d3a75769")
    val rs = Await.result(future, 10.second)
    println(rs)
    rs.isDefined shouldBe(true)
  }

  it should "search a Material By entityID" in {
    val id = "https://www.data.mnhn.fr/data/material/abc1ec5d-237b-5b2f-9998-2a66f93c3553"
    val searchableMaterial =  new SearchableMaterial()
    val future = searchableMaterial.find("https://www.data.mnhn.fr/data/material/abc1ec5d-237b-5b2f-9998-2a66f93c3553")
    val rs = Await.result(future, 10.second)
    println(rs)
  }

  it should "search many Material by entityID" in {
    val searchableMaterial =  new SearchableMaterial()
    val future = searchableMaterial.find(Seq("https://www.data.mnhn.fr/data/material/00aeb0b0-7732-5d94-8517-ee4189adbc8d",
      "https://www.data.mnhn.fr/data/material/0dbf33eb-a44f-5cd8-ba5d-fc5230e15e76", "https://www.data.mnhn.fr/data/material/0f6ef65b-e8aa-5552-a28b-40d854ddd36e"))
    val rs = Await.result(future, 10.second)
    rs.foreach(println(_))
    rs.size shouldBe(3)

  }

  it should "search a person by entityID" in {
    val searchablePerson = new SearchablePerson()
    val future = searchablePerson.find("https://www.data.mnhn.fr/data/person/andrianjaka-m")
    val rs = Await.result(future, 10.second)
    rs.foreach(println(_))
    rs.isDefined shouldBe(true)
  }

  it should "search a taxon by entityID" in {
    val searchableTaxon = new SearchableTaxon()
    val future = searchableTaxon.find("https://www.data.mnhn.fr/data/taxon/0a58185e-65a4-5276-a8f3-cb27f29f9897")
    val rs = Await.result(future, 10.second)
    println(rs)
    rs.isDefined shouldBe(true)
  }

  it should "search a taxon by graphQLArgument" in {
    val graphQLQueryArgument = GraphqlQueryArgument(qs = Some("Urtica granulosa S.F.Blake"))
    val searchableTaxon = new SearchableTaxon()
    val future = searchableTaxon.search(Seq.empty, graphQLQueryArgument, None)
    val rs = Await.result(future, 10.second)
    rs.foreach(r=> println(r.scientificName))
    println(rs.size)
    rs.size should be > 0
  }

  it should "search a taxonAlignment" in {
    val searchableTaxonAlignment = new SearchableTaxonAlignment()
    val future =  searchableTaxonAlignment.find("https://www.data.mnhn.fr/data/taxon-alignement/53d262d8-78cc-5c89-afba-d639b6148c7f_3332455")
    val rs = Await.result(future, 10.second)
    println(rs)
  }

  it should "search a gbif concept" in {
    val searchableConcept = new SearchableConcept()
    val future = searchableConcept.find("https://www.gbif.org/species/3332455")
    val rs = Await.result(future, 10.second)
    println(rs)
  }

}