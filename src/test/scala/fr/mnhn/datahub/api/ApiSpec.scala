package fr.mnhn.datahub.api

import akka.actor.ActorSystem
import com.typesafe.scalalogging.LazyLogging
import org.scalatest.BeforeAndAfterAll
import org.scalatest.concurrent.ScalaFutures
import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ApiSpec extends AnyFlatSpec with Matchers with BeforeAndAfterAll with ScalaFutures with LazyLogging{
  implicit val system = ActorSystem("Api-"+System.currentTimeMillis())
  implicit val ec = system.dispatcher
}