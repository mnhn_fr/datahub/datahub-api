/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api.resolvers

import fr.mnhn.datahub.api.model.{ArchiveItem, Collection, CollectionEvent, CollectionGroup, CollectionItem, ConceptRequestable, Document, EsContext, Geolocation, Geometry, Identification, Material, Person, RelationType, Taxon, TaxonAlignment, TaxonName, Taxref}
import sangria.execution.deferred.{DeferredResolver, Fetcher, HasId}

object FetchersResolver {

  implicit val archiveItemHasUri = HasId[ArchiveItem, String](_.entityId)
  implicit val collectionHasUri = HasId[Collection, String](_.entityId.get)
  implicit val collectionEventHasUri = HasId[CollectionEvent, String](_.entityId.get)
  implicit val collectionGroupHasUri = HasId[CollectionGroup, String](_.entityId.get)

  implicit val documentHasUri = HasId[Document, String](_.entityId)
  implicit val geolocationHasUri: HasId[Geolocation, String] = HasId[Geolocation, String](_.entityId.get)
  implicit val geometryHasUri = HasId[Geometry, String](_.entityId.get)
  implicit val identificationHasUri = HasId[Identification, String](_.entityId.get)
  implicit val materialHasUri = HasId[Material, String](_.entityId.get)
  implicit val personHasUri = HasId[Person, String](_.entityId.get)
  implicit val taxonHasUri = HasId[Taxon, String](_.entityId.get)
  implicit val conceptHasUri = HasId[ConceptRequestable, String](_.entityId.get)

  implicit val taxonNameHasUri = HasId[TaxonName, String](_.entityId.get)
  implicit val taxrefHasUri = HasId[Taxref, String](_.entityId.get)
  implicit val relationTypeUri = HasId[RelationType, String](_.entityId.get)
  implicit val taxonAlignmentTypeUri = HasId[TaxonAlignment, String](_.entityId)

  val documentsFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchableDocument.findSpecificCollectionItem(Some(uris.toList))
  )

  val personsFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchablePerson.find(uris)
  )

  val materialFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchableMaterial.find(uris)
  )

  val taxonFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchableTaxon.find(uris)
  )

  val collectionFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchableCollection.find(uris)
  )

  val identificationFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchableIdentification.find(uris)
  )

  val collectionEventFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchableCollectionEvent.find(uris)
  )

  val geolocationFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchableGeolocation.find(uris)
  )

  val collectionGroupFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchableCollectionGroup.find(uris)
  )

  val geometryFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchableGeometry.find(uris)
  )

  val conceptFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchableConcept.find(uris)
  )

  val archiveItemFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchableArchiveItem.find(uris)
  )

  val taxonNameFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchableTaxonName.find(uris)
  )

  val taxrefFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchableTaxref.find(uris)
  )

  val relationTypeFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchableRelationType.find(uris)
  )

  val taxonAlignmentFetcher = Fetcher(
    (ctx: EsContext, uris: Seq[String]) => ctx.elastic.searchableTaxonAlignment.find(uris)
  )

  val Resolver: DeferredResolver[EsContext] = DeferredResolver.fetchers(
    documentsFetcher, personsFetcher, materialFetcher, taxonFetcher,
    collectionFetcher, collectionEventFetcher, geolocationFetcher,
    geometryFetcher, conceptFetcher, archiveItemFetcher,
    taxonNameFetcher, taxrefFetcher, relationTypeFetcher,
    taxonAlignmentFetcher
  )

}
