package fr.mnhn.datahub.api

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._
import com.mnemotix.synaptix.api.graphql.CORSHandler
import com.typesafe.scalalogging.LazyLogging
import spray.json._
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

import scala.concurrent.Await
import scala.language.postfixOps

object DatahubGraphQLApiServerLauncher extends App with CORSHandler with LazyLogging {
  scala.sys.addShutdownHook(() -> shutdown())

  val PORT = DatahubConfig.serverPort

  implicit val actorSystem = ActorSystem("graphql-server")
  implicit val ec = actorSystem.dispatcher

  import scala.concurrent.duration._
  scala.sys.addShutdownHook(() -> shutdown())

  val graphqlRoute: Route = (post & path("graphql")){
    entity(as[JsValue]) { resquestJson =>
      DatahubGraphQLServer.endpoint(resquestJson)
    }
  }

  val route: Route = corsHandler {
    (graphqlRoute) ~ getFromResource("graphiql.html")
  }

  try {
    Http().newServerAt(DatahubConfig.serverInterface, PORT).bind(route)
  }
  catch {
    case e => throw e
  }

  //Http().bindAndHandle(route, "0.0.0.0", PORT)
  logger.info(s"open a browser with URL: ${DatahubConfig.serverInterface}:$PORT")

  def shutdown(): Unit = {
    actorSystem.terminate()
    Await.result(actorSystem.whenTerminated, 30 seconds)
  }
}