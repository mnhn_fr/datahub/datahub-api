/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api

import akka.http.scaladsl.model.StatusCodes._
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import fr.mnhn.datahub.api.resolvers.FetchersResolver
import sangria.ast.Document
import sangria.execution._
import sangria.parser.QueryParser
import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import fr.mnhn.datahub.api.model.EsContext
import sangria.parser.QueryParser
import spray.json.{JsObject, JsString, JsValue}
import sangria.marshalling.sprayJson._

import scala.concurrent.ExecutionContext
import scala.util.{Failure, Success}

object DatahubGraphQLServer {
  val elasticResolver = new ElasticResolver()

  val errorHandler = ExceptionHandler {
    case (_, AuthenticationException(message)) => HandledException(message)
    case (_, AuthorizationException(message)) => HandledException(message)
  }

  def endpoint(requestJSON: JsValue)(implicit ec: ExecutionContext): Route = {
    val JsObject(fields) = requestJSON
    val JsString(query) = fields("query")

    QueryParser.parse(query) match {
      case Success(queryAst) =>
        // 6
        val operation = fields.get("operationName") collect {
          case JsString(op) => op
        }

        // 7
        val variables = fields.get("variables") match {
          case Some(obj: JsObject) => obj
          case _ => JsObject.empty
        }
        // 8
        complete(executeGraphQLQuery(queryAst, operation, variables))
      case Failure(error) =>
        complete(BadRequest, JsObject("error" -> JsString(error.getMessage)))
    }
  }

  private def executeGraphQLQuery(
                                   query: Document,
                                   operation: Option[String],
                                   vars: JsObject)(implicit ec: ExecutionContext) = {
    // 9
    Executor.execute(
        DatahubSchema.SchemaDefinition, // 10
        query, // 11
        EsContext(elasticResolver), // 12
        deferredResolver = FetchersResolver.Resolver,
        variables = vars, // 13
        operationName = operation, // 14
        exceptionHandler = errorHandler
      ).map(OK -> _)
      .recover {
        case error: QueryAnalysisError => BadRequest -> error.resolveError
        case error: ErrorWithResolver => InternalServerError -> error.resolveError
      }
  }

}
