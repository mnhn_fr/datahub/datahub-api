package fr.mnhn.datahub.api.model

import com.mnemotix.synaptix.api.graphql.helper.DateScalar.DateType
import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.api.model.CollectionItem.CollectionItemType
import fr.mnhn.datahub.api.model.ConceptRequestable.conceptEsConnection
import fr.mnhn.datahub.api.model.Material.materialConnection
import fr.mnhn.datahub.api.resolvers.FetchersResolver
import fr.mnhn.datahub.api.utils.MnxJsonUtils.readSeq
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{Reads, __}
import sangria.macros.derive.{AddFields, DocumentField, ExcludeFields, Interfaces, ObjectTypeDescription, ObjectTypeName, ReplaceField, deriveObjectType}
import sangria.relay.{Connection, ConnectionArgs, ConnectionDefinition, IdentifiableNode, Node}
import sangria.schema._

import java.util.Date
import scala.concurrent.ExecutionContext

case class BibliographicRecord(entityId: String,
                               date: Option[Date],
                               format: Option[String],
                               title: Option[String],
                               topic: Option[String],
                               description: Option[String],
                               note: Option[String],
                               subject: Option[Seq[String]],
                               references: Option[Seq[String]],
                               creator: Option[Seq[String]],
                               author: Option[Seq[String]],
                               publisher: Option[Seq[String]],
                               scientificEditor: Option[Seq[String]],
                               primaryTopic: Option[Seq[String]],
                               originalInfo: Option[String],
                               collection: Option[Seq[String]],
                               physicalDescription: Option[String],

                               dctermsSubject: Option[Seq[String]]
                              ) extends CollectionItem with Node {
  override def id: String = StringUtils.lastPathOfUri(entityId)
}

object BibliographicRecord {
  implicit val ec: ExecutionContext = ExecutionContext.global

  implicit object BibliographicRecordId extends IdentifiableNode[EsContext, BibliographicRecord] {
    def id(ctx: Context[EsContext, BibliographicRecord]): String = ctx.value.id
  }

  implicit val read: Reads[BibliographicRecord] = (
    (__ \ "entityId").read[String] and
      (__ \ "date").readNullable[Date] and
      (__ \ "format").readNullable[String] and
      (__ \ "title").readNullable[String] and
      (__ \ "topic").readNullable[String] and
      (__ \ "description").readNullable[String] and
      (__ \ "note").readNullable[String] and
      (__ \ "subject").readNullable[Seq[String]](readSeq) and
      (__ \ "references").readNullable[Seq[String]](readSeq) and
      (__ \ "creator").readNullable[Seq[String]](readSeq) and
      (__ \ "author").readNullable[Seq[String]](readSeq) and
      (__ \ "publisher").readNullable[Seq[String]](readSeq) and
      (__ \ "scientificEditor").readNullable[Seq[String]](readSeq) and
      (__ \ "primaryTopic").readNullable[Seq[String]](readSeq) and
      (__ \ "originalInfo").readNullable[String] and
      (__ \ "collection").readNullable[Seq[String]](readSeq) and
      (__ \ "physicalDescription").readNullable[String] and
      (__ \ "dctermsSubject").readNullable[Seq[String]](readSeq)
    )(BibliographicRecord.apply _)

  val BibliographicRecordType: ObjectType[Unit, BibliographicRecord] = deriveObjectType(
    Interfaces[Unit, BibliographicRecord](CollectionItemType),
    ObjectTypeName("BibliographicRecord"),
    ObjectTypeDescription("A bibliographic record."),
    DocumentField("entityId", "The ID of the bibliographic record."),
    DocumentField("date", "The date of the bibliographic record."),
    DocumentField("format", "The format of the bibliographic record."),
    DocumentField("title", "The title of the bibliographic record."),
    DocumentField("topic", "The topic of the bibliographic record."),
    DocumentField("description", "The description of the bibliographic record."),
    DocumentField("note", "Additional notes about the bibliographic record."),
    DocumentField("subject", "Subjects related to the bibliographic record."),
    DocumentField("references", "References associated with the bibliographic record."),
    DocumentField("creator", "Creators of the bibliographic record."),
    DocumentField("author", "Authors of the bibliographic record."),
    DocumentField("publisher", "Publishers of the bibliographic record."),
    DocumentField("scientificEditor", "Scientific editors of the bibliographic record."),
    DocumentField("primaryTopic", "Primary topics of the bibliographic record."),
    DocumentField("originalInfo", "Original information about the bibliographic record."),
    DocumentField("collection", "Collections related to the bibliographic record."),
    DocumentField("physicalDescription", "Physical description of the bibliographic record."),
    ExcludeFields("dctermsSubject"),
    AddFields(
      Node.globalIdField,
      Field(
        name = "dctermsSubjects",
        fieldType = conceptEsConnection,
        description = Some("The subject of the collection item."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.dctermsSubject.getOrElse(Nil)
          DeferredValue(FetchersResolver.conceptFetcher.deferSeq(uris)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      )
    )
  )

  val ConnectionDefinition(_, bibliographicRecordConnection) = Connection.definition[EsContext, Connection, BibliographicRecord](
    name = "BibliographicRecord",
    nodeType = BibliographicRecordType
  )

}