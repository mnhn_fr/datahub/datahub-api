/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api.model

import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.api.model.Collection.collectionConnection
import fr.mnhn.datahub.api.model.CollectionEvent.collectionEventConnection
import fr.mnhn.datahub.api.model.ConceptRequestable.conceptEsConnection
import fr.mnhn.datahub.api.model.Identification.identificationConnection
import fr.mnhn.datahub.api.model.Person.personConnection
import fr.mnhn.datahub.api.resolvers.FetchersResolver
import fr.mnhn.datahub.api.utils.MnxJsonUtils.readSeq
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsValue, Json, Reads, Writes, __}
import sangria.macros.derive.{AddFields, DocumentField, ExcludeFields, Interfaces, ObjectTypeDescription, ObjectTypeName, ReplaceField, deriveObjectType}
import sangria.relay.{Connection, ConnectionArgs, ConnectionDefinition, IdentifiableNode, Node}
import sangria.schema.{Context, DeferredValue, Field, ObjectType}

import java.util.Date
import scala.concurrent.ExecutionContext

case class Material(
                     entityId: Option[String],
                     materialEntityID: Option[String],
                     occurrenceID: Option[String],
                     datasetID: Option[String],
                     isPrivate: Option[Boolean],
                     entryNumber: Option[String],
                     catalogNumber: Option[String],
                     autopsyDate: Option[Date],
                     birthDate: Option[Date],
                     boneCondition: Option[String],
                     captivity: Option[String],
                     captivityPlace: Option[String],
                     deathDate: Option[Date],
                     exsiccataAuthor: Option[String],
                     exsiccataNumber: Option[Int],
                     exsiccataPrefix: Option[String],
                     exsiccataSuffix: Option[String],
                     exsiccataTitle: Option[String],
                     fossil: Option[String],
                     individualCount: Option[Int],
                     karyotype2N: Option[String],
                     karyotypeNf: Option[Int],
                     karyotypeNfa: Option[String],
                     materialNature: Option[String],
                     materialEntityRemarks: Option[String],
                     occurrenceRemarks: Option[String],
                     skinCondition: Option[String],
                     stateOfPreservation: Option[String],
                     preparations: Option[String],
                     inCollection: Option[Seq[String]], // Assuming this is a URI represented as a String
                     previouslyInCollection: Option[Seq[String]], // Assuming this is a URI represented as a String
                     hasCollectionEvent: Option[Seq[String]], // Assuming this is a URI represented as a String
                     hasIdentification: Option[Seq[String]], // Assuming this is a URI represented as a String
                     hasDeletation: Option[Date], // Assuming this is a Date
                     hasDomain: Option[Seq[String]]
                   ) extends Node {
  override def id: String = StringUtils.lastPathOfUri(entityId.get)
}


object Material {

   val materialReads: Reads[(Option[String], Option[String], Option[String], Option[String], Option[Boolean], Option[String], Option[String], Option[Date], Option[Date], Option[String], Option[String], Option[String], Option[Date], Option[String], Option[Int], Option[String], Option[String], Option[String], Option[String], Option[Int], Option[String], Option[Int])] = (
    (__ \ "entityId").readNullable[String] and
      (__ \ "materialEntityID").readNullable[String] and
      (__ \ "occurrenceID").readNullable[String] and
      (__ \ "datasetID").readNullable[String] and
      (__ \ "isPrivate").readNullable[Boolean] and
      (__ \ "entryNumber").readNullable[String] and
      (__ \ "catalogNumber").readNullable[String] and
      (__ \ "autopsyDate").readNullable[Date] and
      (__ \ "birthDate").readNullable[Date] and
      (__ \ "boneCondition").readNullable[String] and
      (__ \ "captivity").readNullable[String] and
      (__ \ "captivityPlace").readNullable[String] and
      (__ \ "deathDate").readNullable[Date] and
      (__ \ "exsiccataAuthor").readNullable[String] and
      (__ \ "exsiccataNumber").readNullable[Int] and
      (__ \ "exsiccataPrefix").readNullable[String] and
      (__ \ "exsiccataSuffix").readNullable[String] and
      (__ \ "exsiccataTitle").readNullable[String] and
      (__ \ "fossil").readNullable[String] and
      (__ \ "individualCount").readNullable[Int] and
      (__ \ "karyotype2N").readNullable[String] and
      (__ \ "karyotypeNf").readNullable[Int]
    ).tupled

  val materialReads2: Reads[(Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Date], Option[Seq[String]])] = (
    (__ \ "karyotypeNfa").readNullable[String] and
      (__ \ "materialNature").readNullable[String] and
      (__ \ "materialEntityRemarks").readNullable[String] and
      (__ \ "occurrenceRemarks").readNullable[String] and
      (__ \ "skinCondition").readNullable[String] and
      (__ \ "stateOfPreservation").readNullable[String] and
      (__ \ "preparations").readNullable[String] and
      (__ \ "inCollection").readNullable[Seq[String]](readSeq)  and
      (__ \ "previouslyInCollection").readNullable[Seq[String]](readSeq)  and
      (__ \ "hasCollectionEvent").readNullable[Seq[String]](readSeq)  and
      (__ \ "hasIdentification").readNullable[Seq[String]](readSeq)  and
      (__ \ "hasDeletation").readNullable[Date] and
      (__ \ "hasDomain").readNullable[Seq[String]](readSeq)
  ).tupled

  val f : (
    (Option[String], Option[String], Option[String], Option[String], Option[Boolean], Option[String], Option[String], Option[Date], Option[Date], Option[String], Option[String], Option[String], Option[Date], Option[String], Option[Int], Option[String], Option[String], Option[String], Option[String], Option[Int], Option[String], Option[Int]),
      (Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Date], Option[Seq[String]])
    ) => Material = {
    case (
      (entityId: Option[String],
      materialEntityID: Option[String],
      occurrenceID: Option[String],
      datasetID: Option[String],
      isPrivate: Option[Boolean],
      entryNumber: Option[String],
      catalogNumber: Option[String],
      autopsyDate: Option[Date],
      birthDate: Option[Date],
      boneCondition: Option[String],
      captivity: Option[String],
      captivityPlace: Option[String],
      deathDate: Option[Date],
      exsiccataAuthor: Option[String],
      exsiccataNumber: Option[Int],
      exsiccataPrefix: Option[String],
      exsiccataSuffix: Option[String],
      exsiccataTitle: Option[String],
      fossil: Option[String],
      individualCount: Option[Int],
      karyotype2N: Option[String],
      karyotypeNf: Option[Int]),
      (karyotypeNfa: Option[String],
      materialNature: Option[String],
      materialEntityRemarks: Option[String],
      occurrenceRemarks: Option[String],
      skinCondition: Option[String],
      stateOfPreservation: Option[String],
      preparations: Option[String],
      inCollection: Option[Seq[String]], // Assuming this is a URI represented as a String
      previouslyInCollection: Option[Seq[String]], // Assuming this is a URI represented as a String
      hasCollectionEvent: Option[Seq[String]], // Assuming this is a URI represented as a String
      hasIdentification: Option[Seq[String]], // Assuming this is a URI represented as a String
      hasDeletation: Option[Date], // Assuming this is a Date)
      hasDomain: Option[Seq[String]]
      )) => (Material(
      entityId: Option[String],
      materialEntityID: Option[String],
      occurrenceID: Option[String],
      datasetID: Option[String],
      isPrivate: Option[Boolean],
      entryNumber: Option[String],
      catalogNumber: Option[String],
      autopsyDate: Option[Date],
      birthDate: Option[Date],
      boneCondition: Option[String],
      captivity: Option[String],
      captivityPlace: Option[String],
      deathDate: Option[Date],
      exsiccataAuthor: Option[String],
      exsiccataNumber: Option[Int],
      exsiccataPrefix: Option[String],
      exsiccataSuffix: Option[String],
      exsiccataTitle: Option[String],
      fossil: Option[String],
      individualCount: Option[Int],
      karyotype2N: Option[String],
      karyotypeNf: Option[Int],
      karyotypeNfa: Option[String],
      materialNature: Option[String],
      materialEntityRemarks: Option[String],
      occurrenceRemarks: Option[String],
      skinCondition: Option[String],
      stateOfPreservation: Option[String],
      preparations: Option[String],
      inCollection: Option[Seq[String]], // Assuming this is a URI represented as a String
      previouslyInCollection: Option[Seq[String]], // Assuming this is a URI represented as a String
      hasCollectionEvent: Option[Seq[String]], // Assuming this is a URI represented as a String
      hasIdentification: Option[Seq[String]], // Assuming this is a URI represented as a String
      hasDeletation: Option[Date], // Assuming this is a Date
      hasDomain: Option[Seq[String]]
    )
      )
  }

  implicit val read: Reads[Material] = (materialReads and materialReads2) {
    f
  }

  implicit val materialWrites: Writes[Material] = new Writes[Material] {
    override def writes(material: Material): JsValue = {
      Json.obj(
        "entityId" -> material.entityId,
        "materialEntityID" -> material.materialEntityID,
        "occurrenceID" -> material.occurrenceID,
        "datasetID" -> material.datasetID,
        "isPrivate" -> material.isPrivate,
        "entryNumber" -> material.entryNumber,
        "catalogNumber" -> material.catalogNumber,
        "autopsyDate" -> material.autopsyDate.map(_.getTime),
        "birthDate" -> material.birthDate.map(_.getTime),
        "boneCondition" -> material.boneCondition,
        "captivity" -> material.captivity,
        "captivityPlace" -> material.captivityPlace,
        "deathDate" -> material.deathDate.map(_.getTime),
        "exsiccataAuthor" -> material.exsiccataAuthor,
        "exsiccataNumber" -> material.exsiccataNumber,
        "exsiccataPrefix" -> material.exsiccataPrefix,
        "exsiccataSuffix" -> material.exsiccataSuffix,
        "exsiccataTitle" -> material.exsiccataTitle,
        "fossil" -> material.fossil,
        "individualCount" -> material.individualCount,
        "karyotype2N" -> material.karyotype2N,
        "karyotypeNf" -> material.karyotypeNf,
        "karyotypeNfa" -> material.karyotypeNfa,
        "materialNature" -> material.materialNature,
        "materialEntityRemarks" -> material.materialEntityRemarks,
        "occurrenceRemarks" -> material.occurrenceRemarks,
        "skinCondition" -> material.skinCondition,
        "stateOfPreservation" -> material.stateOfPreservation,
        "preparations" -> material.preparations,
        "inCollection" -> material.inCollection,
        "previouslyInCollection" -> material.previouslyInCollection,
        "hasCollectionEvent" -> material.hasCollectionEvent,
        "hasIdentification" -> material.hasIdentification,
        "hasDeletation" -> material.hasDeletation.map(_.getTime)
      )
    }
  }

  implicit val ec: ExecutionContext = ExecutionContext.global
  import com.mnemotix.synaptix.api.graphql.helper.DateScalar._


  implicit object MaterialId extends IdentifiableNode[EsContext, Material] {
    def id(ctx: Context[EsContext, Material]): String = ctx.value.id
  }


  val MaterialType: ObjectType[EsContext, Material] = deriveObjectType[EsContext, Material](
//    Interfaces(nodeInterface),
    ObjectTypeName("Material"),
    ObjectTypeDescription("Material information."),
    DocumentField("entityId", "The ID of the identification."),
    DocumentField("materialEntityID", "The material entity ID."),
    DocumentField("occurrenceID", "The occurrence ID."),
    DocumentField("datasetID", "The dataset ID."),
    DocumentField("isPrivate", "Indicates whether the material is private."),
    DocumentField("entryNumber", "The entry number of the material."),
    DocumentField("catalogNumber", "The catalog number of the material."),
    DocumentField("autopsyDate", "The autopsy date of the material."),
    DocumentField("birthDate", "The birth date of the material."),
    DocumentField("boneCondition", "The bone condition of the material."),
    DocumentField("captivity", "The captivity status of the material."),
    DocumentField("captivityPlace", "The captivity place of the material."),
    DocumentField("deathDate", "The death date of the material."),
    DocumentField("exsiccataAuthor", "The exsiccata author of the material."),
    DocumentField("exsiccataNumber", "The exsiccata number of the material."),
    DocumentField("exsiccataPrefix", "The exsiccata prefix of the material."),
    DocumentField("exsiccataSuffix", "The exsiccata suffix of the material."),
    DocumentField("exsiccataTitle", "The exsiccata title of the material."),
    DocumentField("fossil", "Indicates whether the material is a fossil."),
    DocumentField("individualCount", "The individual count of the material."),
    DocumentField("karyotype2N", "The karyotype 2N of the material."),
    DocumentField("karyotypeNf", "The karyotype Nf of the material."),
    DocumentField("karyotypeNfa", "The karyotype Nfa of the material."),
    DocumentField("materialNature", "The nature of the material."),
    DocumentField("materialEntityRemarks", "Remarks about the material entity."),
    DocumentField("occurrenceRemarks", "Remarks about the occurrence."),
    DocumentField("skinCondition", "The skin condition of the material."),
    DocumentField("stateOfPreservation", "The state of preservation of the material."),
    DocumentField("preparations", "The preparations of the material."),
    DocumentField("hasDeletation", "The deletion date of the material."),
    ExcludeFields("previouslyInCollection") ,
    AddFields(Node.globalIdField,
      Field(
        name = "previouslyInCollection",
        fieldType = collectionConnection,
        description = Some("The previous collections the material belonged to."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.previouslyInCollection.getOrElse(Nil)
          DeferredValue(FetchersResolver.collectionFetcher.deferSeq(uris)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      ),
      Field(
        name = "collection",
        fieldType = collectionConnection,
        description = Some("The collections the material belongs to."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.inCollection.getOrElse(Nil)
          DeferredValue(FetchersResolver.collectionFetcher.deferSeq(uris)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      ),
      Field(
        name = "identification",
        fieldType = identificationConnection,
        description = Some("The identifications associated with the material."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.hasIdentification.getOrElse(Nil)
          DeferredValue(FetchersResolver.identificationFetcher.deferSeq(uris)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      ),
      Field(
        name = "collectionEvent",
        fieldType = collectionEventConnection,
        description = Some("The collection events associated with the material."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.hasCollectionEvent.getOrElse(Nil)
          DeferredValue(FetchersResolver.collectionEventFetcher.deferSeq(uris)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      ),
      Field(
        name = "domain",
        fieldType = conceptEsConnection,
        description = Some("The Domain of the material"),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.hasCollectionEvent.getOrElse(Nil)
          DeferredValue(FetchersResolver.conceptFetcher.deferSeq(uris)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      )
    )
  )

  val ConnectionDefinition(_, materialConnection) = Connection.definition[EsContext, Connection, Material](
    name = "Material",
    nodeType = MaterialType
  )
}