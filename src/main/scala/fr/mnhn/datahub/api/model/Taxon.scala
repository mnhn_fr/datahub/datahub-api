/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api.model

import com.mnemotix.synaptix.api.graphql.helper.DateScalar.DateType
import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.api.DatahubSchema.nodeInterface
import fr.mnhn.datahub.api.model.CollectionItem.CollectionItemType
import fr.mnhn.datahub.api.model.Person.personConnection
import fr.mnhn.datahub.api.model.TaxonAlignment.taxonAlignmentConnection
import fr.mnhn.datahub.api.model.TaxonName.taxonNameConnection
import fr.mnhn.datahub.api.model.Taxref.taxRefConnection
import fr.mnhn.datahub.api.resolvers.FetchersResolver
import fr.mnhn.datahub.api.utils.MnxJsonUtils.readSeq
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsValue, Json, Reads, Writes, __}
import sangria.relay._
import sangria.macros.derive._
import sangria.schema._

import java.util.Date
import scala.concurrent.ExecutionContext

case class Taxon(
                  entityId: Option[String],
                  taxonID: Option[String],
                  date: Option[Date],
                  kingdom: Option[String],
                  phylum: Option[String],
                  `class`: Option[String], // class is a reserved keyword in Scala, so use class_
                  order: Option[String],
                  family: Option[String],
                  genus: Option[String],
                  taxonomicStatus: Option[String],
                  taxonRemarks: Option[String],
                  scientificNameAuthorship: Option[String],
                  scientificName: Option[String],
                  scientificNameWOAuthorship: Option[String],
                  numFamily: Option[Int],
                  numFamilyAPG: Option[Int],
                  numGenus: Option[Int],
                  taxonRank: Option[String], // Assuming this is a URI represented as a String
                  hasReferenceName: Option[String], // Assuming this is a URI represented as a String
                  hasTaxonAlignment: Option[Seq[String]], // Assuming this is a URI represented as a String
                  nominalTaxonAuthor: Option[Seq[String]], // Assuming this is a URI represented as a String
                  infraspTaxonAuthor: Option[Seq[String]], // Assuming this is a URI represented as a String
                  infraspNominalTaxonAuthor: Option[Seq[String]] // Assuming this is a URI represented as a String
                ) extends Node {
  override def id: String = StringUtils.lastPathOfUri(entityId.get)
}

object Taxon {
  implicit object TaxonId extends IdentifiableNode[EsContext, Taxon] {
    def id(ctx: Context[EsContext, Taxon]): String = ctx.value.id
  }

  val taxonReads1: Reads[(Option[String], Option[String], Option[Date], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[Int], Option[Int], Option[Int], Option[String], Option[String], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]])] = (
    (__ \ "entityId").readNullable[String] and
      (__ \ "taxonID").readNullable[String] and
      (__ \ "date").readNullable[Date] and
      (__ \ "kingdom").readNullable[String] and
      (__ \ "phylum").readNullable[String] and
      (__ \ "class").readNullable[String] and
      (__ \ "order").readNullable[String] and
      (__ \ "family").readNullable[String] and
      (__ \ "genus").readNullable[String] and
      (__ \ "taxonomicStatus").readNullable[String] and
      (__ \ "taxonRemarks").readNullable[String] and
      (__ \ "scientificNameAuthorship").readNullable[String] and
      (__ \ "scientificName").readNullable[String] and
      (__ \ "scientificNameWOAuthorship").readNullable[String] and
      (__ \ "numFamily").readNullable[Int] and
      (__ \ "numFamilyAPG").readNullable[Int] and
      (__ \ "numGenus").readNullable[Int] and
      (__ \ "taxonRank").readNullable[String] and
      (__ \ "hasReferenceName").readNullable[String] and
      (__ \ "hasTaxonAlignment").readNullable[Seq[String]](readSeq) and
      (__ \ "nominalTaxonAuthor").readNullable[Seq[String]](readSeq) and
      (__ \ "infraspTaxonAuthor").readNullable[Seq[String]](readSeq)
  ).tupled

  val taxonRead2: Reads[(Option[Seq[String]])] = (
    (__ \ "infraspNominalTaxonAuthor").readNullable[Seq[String]](readSeq)
  )

  val f: (
    (Option[String], Option[String], Option[Date], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[String], Option[Int], Option[Int], Option[Int], Option[String], Option[String], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]]),
      (Option[Seq[String]])
    ) => Taxon = {
    case (
    (entityId: Option[String],
      taxonID: Option[String],
      date: Option[Date],
      kingdom: Option[String],
      phylum: Option[String],
      `class`: Option[String], // class is a reserved keyword in Scala, so use class_
      order: Option[String],
      family: Option[String],
      genus: Option[String],
      taxonomicStatus: Option[String],
      taxonRemarks: Option[String],
      scientificNameAuthorship: Option[String],
      scientificName: Option[String],
      scientificNameWOAuthorship: Option[String],
      numFamily: Option[Int],
      numFamilyAPG: Option[Int],
      numGenus: Option[Int],
      taxonRank: Option[String], // Assuming this is a URI represented as a String
      hasReferenceName: Option[String], // Assuming this is a URI represented as a String
    hasTaxonAlignment: Option[Seq[String]], // Assuming this is a URI represented as a String
      nominalTaxonAuthor: Option[Seq[String]], // Assuming this is a URI represented as a String
      infraspTaxonAuthor: Option[Seq[String]], // Assuming this is a URI represented as a String
     ),
      ( infraspNominalTaxonAuthor: Option[Seq[String]] )
      ) => Taxon(
      entityId: Option[String],
      taxonID: Option[String],
      date: Option[Date],
      kingdom: Option[String],
      phylum: Option[String],
      `class`: Option[String], // class is a reserved keyword in Scala, so use class_
      order: Option[String],
      family: Option[String],
      genus: Option[String],
      taxonomicStatus: Option[String],
      taxonRemarks: Option[String],
      scientificNameAuthorship: Option[String],
      scientificName: Option[String],
      scientificNameWOAuthorship: Option[String],
      numFamily: Option[Int],
      numFamilyAPG: Option[Int],
      numGenus: Option[Int],
      taxonRank: Option[String], // Assuming this is a URI represented as a String
      hasReferenceName: Option[String], // Assuming this is a URI represented as a String
      hasTaxonAlignment: Option[Seq[String]], // Assuming this is a URI represented as a String
      nominalTaxonAuthor: Option[Seq[String]], // Assuming this is a URI represented as a String
      infraspTaxonAuthor: Option[Seq[String]], // Assuming this is a URI represented as a String
      infraspNominalTaxonAuthor: Option[Seq[String]] // Assuming this is a URI represented as a String
    )
  }

  implicit val read: Reads[Taxon] = (taxonReads1 and taxonRead2) {
    f
  }

  implicit val taxonWrites: Writes[Taxon] = new Writes[Taxon] {
    override def writes(o: Taxon): JsValue = Json.obj(
      "entityId" -> o.entityId,
      "taxonID" -> o.taxonID,
      "date" -> o.date.map(_.getTime), // Convert Date to Unix timestamp
      "kingdom" -> o.kingdom,
      "phylum" -> o.phylum,
      "class" -> o.`class`, // `class` is a reserved keyword, so use backticks
      "order" -> o.order,
      "family" -> o.family,
      "genus" -> o.genus,
      "taxonomicStatus" -> o.taxonomicStatus,
      "taxonRemarks" -> o.taxonRemarks,
      "scientificNameAuthorship" -> o.scientificNameAuthorship,
      "scientificName" -> o.scientificName,
      "scientificNameWOAuthorship" -> o.scientificNameWOAuthorship,
      "numFamily" -> o.numFamily,
      "numFamilyAPG" -> o.numFamilyAPG,
      "numGenus" -> o.numGenus,
      "taxonRank" -> o.taxonRank,
      "hasReferenceName" -> o.hasReferenceName,
      "hasTaxonAlignment" -> o.hasTaxonAlignment,
      "nominalTaxonAuthor" -> o.nominalTaxonAuthor,
      "infraspTaxonAuthor" -> o.infraspTaxonAuthor,
      "infraspNominalTaxonAuthor" -> o.infraspNominalTaxonAuthor
    )
  }

  implicit val ec: ExecutionContext = ExecutionContext.global

  /*
      ExcludeFields("taxonRank", "infraspNominalTaxonAuthor",
      "infraspTaxonAuthor", "hasReferenceName",
      "nominalTaxonAuthor", "hasTaxonAlignment"),
   */

  /*
  val TaxonType: ObjectType[EsContext, Taxon] = deriveObjectType[EsContext, Taxon](
    Interfaces[EsContext, Taxon](nodeInterface),
    ObjectTypeName("Taxon"),
    ObjectTypeDescription("A taxon."),
    DocumentField("entityId","The ID of the taxon."),
    DocumentField("taxonID", "The taxon ID."),
    DocumentField("date", "The date of the taxon."),
    DocumentField("kingdom", "The kingdom of the taxon."),
    DocumentField("phylum", "The phylum of the taxon."),
    DocumentField("class", "The class of the taxon."),
    DocumentField("order", "The order of the taxon."),
    DocumentField("family", "The family of the taxon."),
    DocumentField("genus", "The genus of the taxon."),
    DocumentField("taxonomicStatus", "The taxonomic status of the taxon."),
    DocumentField("taxonRemarks", "Remarks about the taxon."),
    DocumentField("scientificNameAuthorship", "The authorship of the scientific name of the taxon."),
    DocumentField("scientificName", "The scientific name of the taxon."),
    DocumentField("scientificNameWOAuthorship", "The scientific name without authorship of the taxon."),
    DocumentField("numFamily",  "The number of families of the taxon."),
    DocumentField("numFamilyAPG", "The number of families APG of the taxon."),
    DocumentField("numGenus", "The number of genera of the taxon."),

    AddFields(Node.globalIdField,
      Field(
        name = "taxonRank",
        fieldType = taxRefConnection,
        description = Some("The taxon rank."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.taxonRank
          DeferredValue(FetchersResolver.taxrefFetcher.deferSeq(uris.toList)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      ),
      Field(
        name = "infraspNominalTaxonAuthor",
        fieldType = personConnection, // Assuming personConnection is defined elsewhere
        description = Some("Infraspecific nominal taxon authors associated with the taxon."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.infraspNominalTaxonAuthor.getOrElse(Nil)
          DeferredValue(FetchersResolver.personsFetcher.deferSeq(uris)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      ),
      Field(
        name = "infraspTaxonAuthor",
        fieldType = personConnection, // Assuming personConnection is defined elsewhere
        description = Some("Infraspecific taxon authors associated with the taxon."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.infraspTaxonAuthor.getOrElse(Nil)
          DeferredValue(FetchersResolver.personsFetcher.deferSeq(uris)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      ),
      Field(
        name = "referenceName",
        fieldType = taxonNameConnection,
        description = Some("The reference name associated with the taxon."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.hasReferenceName
          DeferredValue(FetchersResolver.taxonNameFetcher.deferSeq(uris.toList)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      ),
      Field(
        name = "nominalTaxonAuthor",
        fieldType = personConnection, // Assuming personConnection is defined elsewhere
        description = Some("Nominal taxon authors associated with the taxon."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.nominalTaxonAuthor.getOrElse(Nil)
          DeferredValue(FetchersResolver.personsFetcher.deferSeq(uris)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      ),
      Field(
        name = "taxonAlignement",
        fieldType = taxonAlignmentConnection,
        description = Some("Taxon alignments associated with the taxon."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.hasTaxonAlignment.getOrElse(Nil)
          DeferredValue(FetchersResolver.taxonAlignmentFetcher.deferSeq(uris)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      )
    )
  )
   */

  val TaxonType: ObjectType[EsContext, Taxon] = ObjectType(
    "Taxon",
    "A taxon.",
    () => fields[EsContext, Taxon](
      Node.globalIdField,
      Field("entityId", OptionType(StringType), Some("The ID of the taxon."), resolve = _.value.entityId),
      Field("taxonID", OptionType(StringType), Some("The taxon ID."), resolve = _.value.taxonID),
      Field("date", OptionType(DateType), Some("The date of the taxon."), resolve = _.value.date),
      Field("kingdom", OptionType(StringType), Some("The kingdom of the taxon."), resolve = _.value.kingdom),
      Field("phylum", OptionType(StringType), Some("The phylum of the taxon."), resolve = _.value.phylum),
      Field("class", OptionType(StringType), Some("The class of the taxon."), resolve = _.value.`class`),
      Field("order", OptionType(StringType), Some("The order of the taxon."), resolve = _.value.order),
      Field("family", OptionType(StringType), Some("The family of the taxon."), resolve = _.value.family),
      Field("genus", OptionType(StringType), Some("The genus of the taxon."), resolve = _.value.genus),
      Field("taxonomicStatus", OptionType(StringType), Some("The taxonomic status of the taxon."), resolve = _.value.taxonomicStatus),
      Field("taxonRemarks", OptionType(StringType), Some("Remarks about the taxon."), resolve = _.value.taxonRemarks),
      Field("scientificNameAuthorship", OptionType(StringType), Some("The authorship of the scientific name of the taxon."), resolve = _.value.scientificNameAuthorship),
      Field("scientificName", OptionType(StringType), Some("The scientific name of the taxon."), resolve = _.value.scientificName),
      Field("scientificNameWOAuthorship", OptionType(StringType), Some("The scientific name without authorship of the taxon."), resolve = _.value.scientificNameWOAuthorship),
      Field("numFamily", OptionType(IntType), Some("The number of families of the taxon."), resolve = _.value.numFamily),
      Field("numFamilyAPG", OptionType(IntType), Some("The number of families APG of the taxon."), resolve = _.value.numFamilyAPG),
      Field("numGenus", OptionType(IntType), Some("The number of genera of the taxon."), resolve = _.value.numGenus),
      Field("taxonRank", fieldType = OptionType(StringType), description = Some("The taxon rank."), resolve = _.value.taxonRank),
      Field(
          name = "infraspNominalTaxonAuthor",
          fieldType = personConnection,
          description = Some("Infraspecific nominal taxon authors associated with the taxon."),
          arguments = Connection.Args.All,
          resolve = ctx => {
            val uris = ctx.value.infraspNominalTaxonAuthor.getOrElse(Nil)
            DeferredValue(FetchersResolver.personsFetcher.deferSeq(uris)).map { fetched =>
              Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
            }
          }
        ),
        Field(
          name = "infraspTaxonAuthor",
          fieldType = personConnection,
          description = Some("Infraspecific taxon authors associated with the taxon."),
          arguments = Connection.Args.All,
          resolve = ctx => {
            val uris = ctx.value.infraspTaxonAuthor.getOrElse(Nil)
            DeferredValue(FetchersResolver.personsFetcher.deferSeq(uris)).map { fetched =>
              Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
            }
          }
        ),
        Field(
          name = "referenceName",
          fieldType = taxonNameConnection,
          description = Some("The reference name associated with the taxon."),
          arguments = Connection.Args.All,
          resolve = ctx => {
            val uris = ctx.value.hasReferenceName
            DeferredValue(FetchersResolver.taxonNameFetcher.deferSeq(uris.toList)).map { fetched =>
              Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
            }
          }
        ),
        Field(
          name = "nominalTaxonAuthor",
          fieldType = personConnection,
          description = Some("Nominal taxon authors associated with the taxon."),
          arguments = Connection.Args.All,
          resolve = ctx => {
            val uris = ctx.value.nominalTaxonAuthor.getOrElse(Nil)
            DeferredValue(FetchersResolver.personsFetcher.deferSeq(uris)).map { fetched =>
              Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
            }
          }
        ),
        Field(
          name = "taxonAlignment",
          fieldType = taxonAlignmentConnection,
          description = Some("Taxon alignments associated with the taxon."),
          arguments = Connection.Args.All,
          resolve = ctx => {
            val uris = ctx.value.hasTaxonAlignment.getOrElse(Nil)
            DeferredValue(FetchersResolver.taxonAlignmentFetcher.deferSeq(uris)).map { fetched =>
              Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
            }
          }
        )
    )
    //    ExcludeFields("taxonRank", "infraspNominalTaxonAuthor", "infraspTaxonAuthor", "hasReferenceName", "nominalTaxonAuthor", "hasTaxonAlignment"),
  )


  val ConnectionDefinition(_, taxonConnection) = Connection.definition[EsContext, Connection, Taxon](
    name = "Taxon",
    nodeType = TaxonType
  )
}