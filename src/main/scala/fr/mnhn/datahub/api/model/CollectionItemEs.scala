/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api.model

import fr.mnhn.datahub.api.utils.MnxJsonUtils.readSeq
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{Reads, __}
import sangria.relay.{Connection, ConnectionDefinition}

import java.util.Date

case class CollectionItemEs(entityId: String,
                            date: Option[Date],
                            format: Option[String],
                            title: Option[String],
                            topic: Option[String],
                            description: Option[String],
                            note: Option[String],
                            subject: Option[Seq[String]],
                            references: Option[Seq[String]],
                            originalInfo: Option[String],                     // archive item
                            acquisitionInfo: Option[String],                    // archive item
                            physicalDescription: Option[String],                    // archive item
                            containsArchiveItem: Option[Seq[String]],                    // archive item
                            publisherName: Option[String],  // Document
                            collection: Option[Seq[String]],      // sourceDataset
                            creator: Option[Seq[String]],
                            author: Option[Seq[String]],
                            publisher: Option[Seq[String]],
                            scientificEditor: Option[Seq[String]],
                            primaryTopic: Option[Seq[String]],
                            dctermsSubject: Option[Seq[String]]
                           ) extends CollectionItem

object CollectionItemEs {
  implicit val collectionItemReads: Reads[CollectionItemEs] = (
    (__ \ "entityId").read[String] and
      (__ \ "date").readNullable[Date] and
      (__ \ "format").readNullable[String] and
      (__ \ "title").readNullable[String] and
      (__ \ "topic").readNullable[String] and
      (__ \ "description").readNullable[String] and
      (__ \ "note").readNullable[String] and
      (__ \ "subject").readNullable[Seq[String]](readSeq) and
      (__ \ "references").readNullable[Seq[String]](readSeq) and
      (__ \ "originalInfo").readNullable[String] and
      (__ \ "acquisitionInfo").readNullable[String] and
      (__ \ "physicalDescription").readNullable[String] and
      (__ \ "containsArchiveItem").readNullable[Seq[String]](readSeq) and
      (__ \ "publisherName").readNullable[String] and
      (__ \ "collection").readNullable[Seq[String]](readSeq) and
      (__ \ "creator").readNullable[Seq[String]](readSeq) and
      (__ \ "author").readNullable[Seq[String]](readSeq) and
      (__ \ "publisher").readNullable[Seq[String]](readSeq) and
      (__ \ "scientificEditor").readNullable[Seq[String]](readSeq) and
      (__ \ "primaryTopic").readNullable[Seq[String]](readSeq) and
      (__ \ "dctermsSubject").readNullable[Seq[String]](readSeq)
    )(CollectionItemEs.apply _)
}