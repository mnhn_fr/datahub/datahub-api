package fr.mnhn.datahub.api.model

import com.mnemotix.synaptix.core.utils.StringUtils
import play.api.libs.json.Json
import sangria.macros.derive.{AddFields, DocumentField, Interfaces, ObjectTypeDescription, ObjectTypeName, deriveObjectType}
import sangria.relay.{Connection, ConnectionDefinition, IdentifiableNode, Node}
import sangria.schema.{Context, ObjectType}

case class MaterialRelationship(
                               entityId: Option[String],
                               relationObject: Option[String],
                               relationSubject: Option[String],
                               relationType: Option[String]
                               ) extends Node {
  override def id: String = StringUtils.lastPathOfUri(entityId.get)
}

object MaterialRelationship {
  implicit lazy val format = Json.format[MaterialRelationship]


    implicit object MaterialRelationshipId extends IdentifiableNode[EsContext, MaterialRelationship] {
    def id(ctx: Context[EsContext, MaterialRelationship]): String = ctx.value.id
  }


  val MaterialRelationshipType: ObjectType[EsContext, MaterialRelationship] = deriveObjectType[EsContext, MaterialRelationship](
  //  Interfaces(nodeInterface),
    ObjectTypeName("MaterialRelationship"),
    ObjectTypeDescription("Material Relationship information."),
    DocumentField("entityId", "The ID of the material relationship."),
    DocumentField("relationObject", "The material object of the relation."),
    DocumentField("relationSubject", "The material subject of the relation."),
    DocumentField("relationType", "The relation Type ID."),
    AddFields(Node.globalIdField)
  )


  val ConnectionDefinition(_, materialRelationShipConnection) = Connection.definition[EsContext, Connection, MaterialRelationship](
    name = "MaterialRelationship",
    nodeType = MaterialRelationshipType
  )


}
