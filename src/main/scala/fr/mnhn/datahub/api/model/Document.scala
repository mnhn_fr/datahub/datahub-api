/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api.model

import com.mnemotix.synaptix.api.graphql.helper.DateScalar.DateType
import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.api.DatahubSchema.nodeInterface
import fr.mnhn.datahub.api.model.CollectionItem.CollectionItemType
import fr.mnhn.datahub.api.model.ConceptRequestable.conceptEsConnection
import fr.mnhn.datahub.api.resolvers.FetchersResolver
import fr.mnhn.datahub.api.utils.MnxJsonUtils.readSeq
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{Reads, __}
import sangria.macros.derive.{AddFields, DocumentField, ExcludeFields, Interfaces, ObjectTypeDescription, ObjectTypeName, ReplaceField, deriveObjectType}
import sangria.relay.{Connection, ConnectionArgs, ConnectionDefinition, IdentifiableNode, Node}
import sangria.schema._

import java.util.Date
import scala.concurrent.ExecutionContext

case class Document(entityId: String,
                    date: Option[Date],
                    format: Option[String],
                    title: Option[String],
                    topic: Option[String],
                    description: Option[String],
                    note: Option[String],
                    subject: Option[Seq[String]],
                    references: Option[Seq[String]],
                    creator: Option[Seq[String]],
                    author: Option[Seq[String]],
                    publisher: Option[Seq[String]],
                    scientificEditor: Option[Seq[String]],
                    primaryTopic: Option[Seq[String]],
                    originalInfo: Option[String],
                    publisherName: Option[String],  // Document
                    collection: Option[Seq[String]],

                    dctermsSubject: Option[Seq[String]]
                   ) extends CollectionItem with Node {
  override def id: String = StringUtils.lastPathOfUri(entityId)
}

object Document {
    implicit val ec: ExecutionContext = ExecutionContext.global

  implicit object DocumentId extends IdentifiableNode[EsContext, Document] {
    def id(ctx: Context[EsContext, Document]): String = ctx.value.id
  }

    implicit val read: Reads[Document] = (
    (__ \ "entityId").read[String] and
      (__ \ "date").readNullable[Date] and
      (__ \ "format").readNullable[String] and
      (__ \ "title").readNullable[String] and
      (__ \ "topic").readNullable[String] and
      (__ \ "description").readNullable[String] and
      (__ \ "note").readNullable[String] and
      (__ \ "subject").readNullable[Seq[String]](readSeq) and
      (__ \ "references").readNullable[Seq[String]](readSeq) and
      (__ \ "creator").readNullable[Seq[String]](readSeq) and
      (__ \ "author").readNullable[Seq[String]](readSeq) and
      (__ \ "publisher").readNullable[Seq[String]](readSeq) and
      (__ \ "scientificEditor").readNullable[Seq[String]](readSeq) and
      (__ \ "primaryTopic").readNullable[Seq[String]](readSeq) and
       (__ \ "originalInfo").readNullable[String] and
      (__ \ "publisherName").readNullable[String] and
       (__ \ "collection").readNullable[Seq[String]](readSeq) and
      (__ \ "dctermsSubject").readNullable[Seq[String]](readSeq)
      )(Document.apply _)


  val DocumentType: ObjectType[Unit, Document] = deriveObjectType(
    ObjectTypeName("Document"),
    ObjectTypeDescription("A document"),
    Interfaces[Unit, Document](CollectionItemType),
    Interfaces(nodeInterface),
    DocumentField("entityId", "The ID of the bibliographic record."),
    DocumentField("date", "The date of the bibliographic record."),
    DocumentField("format", "The format of the bibliographic record."),
    DocumentField("title", "The title of the bibliographic record."),
    DocumentField("topic", "The topic of the bibliographic record."),
    DocumentField("description", "The description of the bibliographic record."),
    DocumentField("note", "Additional notes about the bibliographic record."),
    DocumentField("subject", "Subjects related to the bibliographic record."),
    DocumentField("references", "References associated with the bibliographic record."),
    DocumentField("creator", "Creators of the bibliographic record."),
    DocumentField("author", "Authors of the bibliographic record."),
    DocumentField("publisher", "Publishers of the bibliographic record."),
    DocumentField("scientificEditor", "Scientific editors of the bibliographic record."),
    DocumentField("primaryTopic", "Primary topics of the bibliographic record."),
    DocumentField("publisherName", "The name of the publisher of the document."),
    DocumentField("collection", "The collection of the collection item."),
    ExcludeFields("dctermsSubject"),
    AddFields(
      Node.globalIdField,
      Field(
        name = "dctermsSubjects",
        fieldType = conceptEsConnection,
        description = Some("The subject of the collection item."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.dctermsSubject.getOrElse(Nil)
          DeferredValue(FetchersResolver.conceptFetcher.deferSeq(uris)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      )
    )
  )

 val ConnectionDefinition(_, documentConnection) = Connection.definition[EsContext, Connection, Document](
   name = "Document",
   nodeType = DocumentType
 )
}