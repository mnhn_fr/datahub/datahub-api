/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api.model

import fr.mnhn.datahub.api.utils.MnxJsonUtils.readSeq
import fr.mnhn.datahub.api.utils.RDFUtils.nameSpaceMap
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsValue, Json, Reads, Writes, __}
import sangria.relay.{Connection, ConnectionDefinition}
import sangria.schema.{Field, InterfaceType, fields}
import sangria.schema._

import java.util.Date

trait CollectionItem {
  def entityId: String
  def date: Option[Date]
  def format: Option[String]
  def title: Option[String]
  def topic: Option[String]
  def description: Option[String]
  def note: Option[String]
  def subject: Option[Seq[String]]
  def references: Option[Seq[String]]
  def originalInfo: Option[String]
  def author: Option[Seq[String]]
  def publisher: Option[Seq[String]]
  def  scientificEditor: Option[Seq[String]]
  def primaryTopic: Option[Seq[String]]
  def creator: Option[Seq[String]]
  def collection: Option[Seq[String]]                   // sourceDataset

  def dctermsSubject: Option[Seq[String]] // subject
}

object CollectionItem {

  import com.mnemotix.synaptix.api.graphql.helper.DateScalar._

  val CollectionItemType  = InterfaceType(
    s"CollectionItem",
    "A Collection Item",
    () => fields[Unit, CollectionItem](
      Field("entityId", StringType, Some("The ID of the collection item."), resolve = _.value.entityId),
      Field("date", OptionType(DateType), Some("The date of the collection item."), resolve = _.value.date),
      Field("format", OptionType(StringType), Some("The format of the collection item."), resolve = _.value.format),
      Field("title", OptionType(StringType), Some("The title of the collection item."), resolve = _.value.title),
      Field("topic", OptionType(StringType), Some("The topic of the collection item."), resolve = _.value.topic),
      Field("description", OptionType(StringType), Some("The description of the collection item."), resolve = _.value.description),
      Field("note", OptionType(StringType), Some("The note of the collection item."), resolve = _.value.note),
      Field("subject", OptionType(ListType(StringType)), Some("The subjects of the collection item."), resolve = _.value.subject.getOrElse(Nil)),
      Field("references", OptionType(ListType(StringType)), Some("The references of the collection item."), resolve = _.value.references.getOrElse(Nil)),
      Field("creator", OptionType(ListType(StringType)), Some("The creators of the collection item."), resolve = _.value.creator.getOrElse(Nil)),
      Field("author", OptionType(ListType(StringType)), Some("The authors of the collection item."), resolve = _.value.author.getOrElse(Nil)),
      Field("publisher", OptionType(ListType(StringType)), Some("The publishers of the collection item."), resolve = _.value.publisher.getOrElse(Nil)),
      Field("scientificEditor", OptionType(ListType(StringType)), Some("The scientific editors of the collection item."), resolve = _.value.scientificEditor.getOrElse(Nil)),
      Field("primaryTopic", OptionType(ListType(StringType)), Some("The primary topics of the collection item."), resolve = _.value.primaryTopic.getOrElse(Nil)),
      Field("collection", OptionType(ListType(StringType)), Some("The collection of the collection item."), resolve = _.value.primaryTopic.getOrElse(Nil)),
      Field("dctermsSubject", OptionType(ListType(StringType)), Some("The subject of the collection item."), resolve = _.value.dctermsSubject.getOrElse(Nil))
    )
  )

  val ConnectionDefinition(_, collectionItemConnection) = Connection.definition[EsContext, Connection, CollectionItem](
    name = "CollectionItem",
    nodeType = CollectionItemType
  )
}