/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api.model

import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.api.DatahubSchema.nodeInterface
import fr.mnhn.datahub.api.model.Geolocation.geolocationConnection
import fr.mnhn.datahub.api.resolvers.FetchersResolver
import play.api.libs.json.Json
import sangria.macros.derive.{AddFields, DocumentField, ExcludeFields, Interfaces, ObjectTypeDescription, ObjectTypeName, ReplaceField, deriveObjectType}
import sangria.relay.{Connection, ConnectionArgs, ConnectionDefinition, IdentifiableNode, Node}
import sangria.schema.{Context, DeferredValue, Field, ObjectType}

import java.util.Date
import scala.concurrent.ExecutionContext

case class CollectionEvent(
                            entityId: Option[String],
                            eventID: Option[String],
                            dataOrigin: Option[String],
                            duplicateCount: Option[Int],
                            duplicateDestination: Option[String],
                            eventDate: Option[Date],
                            earliestDateCollected: Option[Date],
                            latestDateCollected: Option[Date],
                            verbatimEventDate: Option[String],
                            fieldNumber: Option[String],
                            host: Option[String],
                            oldHarvestIdenfifier: Option[String],
                            partCount: Option[Int],
                            eventRemarks: Option[String],
                            stationNumber: Option[String],
                            usage: Option[String],
                            hasGeolocation: Option[String],
                            recordedBy: Option[String]
                          ) extends Node {
  override def id: String = StringUtils.lastPathOfUri(entityId.get)
}

object CollectionEvent {
  implicit lazy val format = Json.format[CollectionEvent]

  implicit val ec: ExecutionContext = ExecutionContext.global

   implicit object CollectionEventId extends IdentifiableNode[EsContext, CollectionEvent] {
    def id(ctx: Context[EsContext, CollectionEvent]): String = ctx.value.id
  }

  import com.mnemotix.synaptix.api.graphql.helper.DateScalar._

  val CollectionEventType: ObjectType[EsContext, CollectionEvent] = deriveObjectType[EsContext, CollectionEvent](
   //Interfaces(nodeInterface),
    ObjectTypeName("CollectionEvent"),
    ObjectTypeDescription("Collection event information."),
    DocumentField("entityId", "The ID of the collection event."),
    DocumentField("eventID", "The event ID."),
    DocumentField("dataOrigin", "The data origin."),
    DocumentField("duplicateCount", "The duplicate count."),
    DocumentField("duplicateDestination", "The duplicate destination."),
    DocumentField("eventDate", "The date of the event."),
    DocumentField("earliestDateCollected", "The earliest date the collection was made."),
    DocumentField("latestDateCollected", "The latest date the collection was made."),
    DocumentField("verbatimEventDate", "The verbatim event date."),
    DocumentField("fieldNumber", "The field number."),
    DocumentField("host", "The host."),
    DocumentField("oldHarvestIdenfifier", "The old harvest identifier."),
    DocumentField("partCount", "The part count."),
    DocumentField("eventRemarks", "Remarks about the collection event."),
    DocumentField("stationNumber", "The station number."),
    DocumentField("usage", "The usage."),
    DocumentField("recordedBy", "The individual who recorded the collection event."),
    ExcludeFields("hasGeolocation"),
    AddFields(
      Node.globalIdField,
      Field(
        name = "geolocation",
        fieldType = geolocationConnection,
        description = Some("Indicates whether the collection event has geolocation."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.hasGeolocation
          DeferredValue(FetchersResolver.geolocationFetcher.deferSeq(uris.toSeq)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      )
    ),
    //AddFields[EsContext, CollectionEvent](Node.globalIdField),

  )

  val ConnectionDefinition(_, collectionEventConnection) = Connection.definition[EsContext, Connection, CollectionEvent](
    name = "CollectionEvent",
    nodeType = CollectionEventType
  )
}