/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api.model

import com.mnemotix.synaptix.core.utils.StringUtils
import play.api.libs.json.Json
import sangria.macros.derive.{AddFields, DocumentField, Interfaces, ObjectTypeDescription, ObjectTypeName, deriveObjectType}
import sangria.relay.{Connection, ConnectionDefinition, IdentifiableNode, Node}
import sangria.schema.{Context, ObjectType}

case class Geometry(
                     entityId: Option[String],
                     decimalLatitude: Option[Double],
                     decimalLongitude: Option[Double],
                     verbatimCoordinates: Option[String],
                     verbatimCoordinatesSystem: Option[String],
                     coordinatePrecision: Option[String],
                     georeferenceSources: Option[String],
                     coordinatesEstimated: Option[Boolean],
                     asGeoJSON: Option[String],
                     alt: Option[Double],
                     minimumElevationInMeters: Option[Double],
                     maximumElevationInMeters: Option[Double],
                     altitudeUnit: Option[String],
                     altitudeAccuracy: Option[String],
                     altitudeRemarks: Option[String],
                     verbatimDepth: Option[String],
                     minimumDepthInMeters: Option[String],
                     maximumDepthInMeters: Option[String],
                     geodeticDatum: Option[String]
                   ) extends Node {
  override def id: String = StringUtils.lastPathOfUri(entityId.get)
}

object Geometry {
  implicit lazy val format = Json.format[Geometry]
  implicit object GeometryId extends IdentifiableNode[EsContext, Geometry] {
    def id(ctx: Context[EsContext, Geometry]): String = ctx.value.id
  }

  val GeometryType: ObjectType[Unit, Geometry] = deriveObjectType[Unit, Geometry](
   // Interfaces(nodeInterface),
    ObjectTypeName("Geometry"),
    ObjectTypeDescription("Geometry information."),
      DocumentField("entityId", "The ID of the geometry."),
      DocumentField("decimalLatitude", "The decimal latitude."),
      DocumentField("decimalLongitude", "The decimal longitude."),
      DocumentField("verbatimCoordinates", "The verbatim coordinates."),
      DocumentField("verbatimCoordinatesSystem", "The verbatim coordinate system."),
      DocumentField("coordinatePrecision", "The coordinate precision."),
      DocumentField("georeferenceSources", "The georeference sources."),
      DocumentField("coordinatesEstimated", "Indicates if the coordinates are estimated."),
      DocumentField("asGeoJSON", "The geometry in GeoJSON format."),
      DocumentField("alt", "The altitude."),
      DocumentField("minimumElevationInMeters", "The minimum elevation in meters."),
      DocumentField("maximumElevationInMeters", "The maximum elevation in meters."),
      DocumentField("altitudeUnit", "The unit of altitude."),
      DocumentField("altitudeAccuracy", "The accuracy of altitude."),
      DocumentField("altitudeRemarks", "Remarks about altitude."),
      DocumentField("verbatimDepth", "The verbatim depth."),
      DocumentField("minimumDepthInMeters", "The minimum depth in meters."),
      DocumentField("maximumDepthInMeters", "The maximum depth in meters."),
      DocumentField("geodeticDatum", "The geodetic datum."),
    AddFields(Node.globalIdField)
  )

  val ConnectionDefinition(_, geometryConnection) = Connection.definition[EsContext, Connection, Geometry](
    name = "Geometry",
    nodeType = GeometryType
  )
}