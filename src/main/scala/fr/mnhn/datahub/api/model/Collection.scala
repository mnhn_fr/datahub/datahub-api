/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api.model

import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.api.model.CollectionGroup.collectionGroupConnection
import fr.mnhn.datahub.api.model.Person.personConnection
import fr.mnhn.datahub.api.resolvers.FetchersResolver
import play.api.libs.json.Json
import sangria.macros.derive.{AddFields, DocumentField, ExcludeFields, Interfaces, ObjectTypeDescription, ObjectTypeName, ReplaceField, deriveObjectType}
import sangria.relay.{Connection, ConnectionArgs, ConnectionDefinition, IdentifiableNode, Node}
import sangria.schema.{Context, DeferredValue, Field, ObjectType}

import scala.concurrent.ExecutionContext

case class Collection(
                     entityId: Option[String],
                     collectionID: Option[String],
                     label: Option[String],
                     collectionCode: Option[String],
                     description: Option[String],
                     hasInstitution: Option[String],
                     hasCollectionGroup: Option[String]
                     ) extends Node {
  override def id: String = StringUtils.lastPathOfUri(entityId.get)
}

object Collection {
  implicit lazy val format = Json.format[Collection]

  implicit val ec: ExecutionContext = ExecutionContext.global
  implicit object CollectionId extends IdentifiableNode[EsContext, Collection] {
    def id(ctx: Context[EsContext, Collection]): String = ctx.value.id
  }

  val CollectionType: ObjectType[EsContext, Collection] = deriveObjectType[EsContext, Collection](
   // Interfaces(nodeInterface),
    ObjectTypeName("Collection"),
    ObjectTypeDescription("Collection information."),
    DocumentField("entityId", "The ID of the collection."),
    DocumentField("collectionID", "The collection ID."),
    DocumentField("label", "The label of the collection."),
    DocumentField("collectionCode", "The code of the collection."),
    DocumentField("description", "Description of the collection."),
    DocumentField("hasInstitution", "The institution associated with the collection."),
    //DocumentField("hasCollectionGroup", "The group associated with the collection."),
    AddFields[EsContext, Collection](Node.globalIdField,
      Field(
        name = "collectionGroup",
        fieldType = collectionGroupConnection,
        description = Some("The group associated with the collection."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.hasCollectionGroup
          DeferredValue(FetchersResolver.collectionGroupFetcher.deferSeq(uris.toSeq)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      )
    ),
    ExcludeFields("hasCollectionGroup")
  )
    val ConnectionDefinition(_, collectionConnection) = Connection.definition[EsContext, Connection, Collection](
    name = "Collection",
    nodeType = CollectionType
  )
}