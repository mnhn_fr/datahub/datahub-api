package fr.mnhn.datahub.api.model

import fr.mnhn.datahub.api.utils.MnxJsonUtils.readSeq
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{Reads, __}
import sangria.relay.{Connection, ConnectionDefinition}
import sangria.schema.{Field, InterfaceType, fields}
import sangria.schema._

trait Concept {
  def altLabel: Option[Seq[String]]

  def broader: Option[Seq[String]]

  def broaderMatch: Option[Seq[String]]

  def broaderTransitive: Option[Seq[String]]

  def broaderTransitivePrefLabels: Option[Seq[String]]

  def changeNote: Option[Seq[String]]

  def closeMatch: Option[Seq[String]]

  def collection: Option[Seq[String]]

  def comment: Option[Seq[String]]

  def definition: Option[String]

  def entityId: Option[String]

  def exactMatch: Option[Seq[String]]

  def example: Option[String]

  def historyNote: Option[String]

  def inScheme: Option[Seq[String]]

  def narrowerMatch: Option[Seq[String]]

  def notation: Option[String]

  def note: Option[Seq[String]]

  def percoLabel: Option[String]

  def prefLabel: Option[Seq[String]]

  def related: Option[Seq[String]]

  def relatedMatch: Option[Seq[String]]

  def schemePrefLabel: Option[String]

  def scopeNote: Option[String]

  def seeAlso: Option[String]

  def topConceptOf: Option[Seq[String]]

  def types: Option[Seq[String]]

  def vocabularyPrefLabel: Option[String]
}

object Concept {
  val ConceptType: InterfaceType[Unit, Concept] = InterfaceType(
    "Concept",
    "A concept interface.",
    fields[Unit, Concept](
      Field("altLabel", OptionType(ListType(StringType)), Some("Alternative labels of the concept."), resolve = _.value.altLabel.getOrElse(Nil)),
      Field("broader", OptionType(ListType(StringType)), Some("Broader concepts of the concept."), resolve = _.value.broader.getOrElse(Nil)),
      Field("broaderMatch", OptionType(ListType(StringType)), Some("Broader match concepts of the concept."), resolve = _.value.broaderMatch.getOrElse(Nil)),
      Field("broaderTransitive", OptionType(ListType(StringType)), Some("Transitively broader concepts of the concept."), resolve = _.value.broaderTransitive.getOrElse(Nil)),
      Field("broaderTransitivePrefLabels", OptionType(ListType(StringType)), Some("Transitively broader preferred labels of the concept."), resolve = _.value.broaderTransitivePrefLabels.getOrElse(Nil)),
      Field("changeNote", OptionType(ListType(StringType)), Some("Change notes of the concept."), resolve = _.value.changeNote.getOrElse(Nil)),
      Field("closeMatch", OptionType(ListType(StringType)), Some("Close match concepts of the concept."), resolve = _.value.closeMatch.getOrElse(Nil)),
      Field("collection", OptionType(ListType(StringType)), Some("Collections associated with the concept."), resolve = _.value.collection.getOrElse(Nil)),
      Field("comment", OptionType(ListType(StringType)), Some("Comments on the concept."), resolve = _.value.comment.getOrElse(Nil)),
      Field("definition", OptionType(StringType), Some("Definition of the concept."), resolve = _.value.definition),
      Field("entityId", OptionType(StringType), Some("The ID of the concept."), resolve = _.value.entityId),
      Field("exactMatch", OptionType(ListType(StringType)), Some("Exact match concepts of the concept."), resolve = _.value.exactMatch.getOrElse(Nil)),
      Field("example", OptionType(StringType), Some("Example of the concept."), resolve = _.value.example),
      Field("historyNote", OptionType(StringType), Some("History note of the concept."), resolve = _.value.historyNote),
      Field("inScheme", OptionType(ListType(StringType)), Some("Schemes in which the concept is included."), resolve = _.value.inScheme.getOrElse(Nil)),
      Field("narrowerMatch", OptionType(ListType(StringType)), Some("Narrower match concepts of the concept."), resolve = _.value.narrowerMatch.getOrElse(Nil)),
      Field("notation", OptionType(StringType), Some("Notation of the concept."), resolve = _.value.notation),
      Field("note", OptionType(ListType(StringType)), Some("Notes on the concept."), resolve = _.value.note.getOrElse(Nil)),
      Field("percoLabel", OptionType(StringType), Some("Percolator label of the concept."), resolve = _.value.percoLabel),
      Field("prefLabel", OptionType(ListType(StringType)), Some("Preferred labels of the concept."), resolve = _.value.prefLabel.getOrElse(Nil)),
      Field("related", OptionType(ListType(StringType)), Some("Related concepts of the concept."), resolve = _.value.related.getOrElse(Nil)),
      Field("relatedMatch", OptionType(ListType(StringType)), Some("Related match concepts of the concept."), resolve = _.value.relatedMatch.getOrElse(Nil)),
      Field("schemePrefLabel", OptionType(StringType), Some("Preferred label of the scheme associated with the concept."), resolve = _.value.schemePrefLabel),
      Field("scopeNote", OptionType(StringType), Some("Scope note of the concept."), resolve = _.value.scopeNote),
      Field("seeAlso", OptionType(StringType), Some("See also link of the concept."), resolve = _.value.seeAlso),
      Field("topConceptOf", OptionType(ListType(StringType)), Some("Top concepts of the concept."), resolve = _.value.topConceptOf.getOrElse(Nil)),
      Field("types", OptionType(ListType(StringType)), Some("Types associated with the concept."), resolve = _.value.types.getOrElse(Nil)),
      Field("vocabularyPrefLabel", OptionType(StringType), Some("Preferred label of the vocabulary associated with the concept."), resolve = _.value.vocabularyPrefLabel)
    )
  )
}