/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api.model

import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.api.DatahubSchema.nodeInterface
import fr.mnhn.datahub.api.model.Document.documentConnection
import fr.mnhn.datahub.api.model.Taxon.taxonConnection
import fr.mnhn.datahub.api.resolvers.FetchersResolver
import fr.mnhn.datahub.api.utils.MnxJsonUtils.readSeq
import fr.mnhn.datahub.api.utils.RDFUtils.nameSpaceMap
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsValue, Json, Reads, Writes, __}
import sangria.macros.derive.{AddFields, DocumentField, ExcludeFields, Interfaces, ObjectTypeDescription, ObjectTypeName, ReplaceField, deriveObjectType}
import sangria.relay.{Connection, ConnectionArgs, ConnectionDefinition, IdentifiableNode, Node}
import sangria.schema._

import java.util.Date
import scala.concurrent.ExecutionContext

case class Person(
                   entityId: Option[String],
                   lastName: Option[Seq[String]],
                   firstName: Option[Seq[String]],
                   nick: Option[Seq[String]],
                   name: Option[Seq[String]],
                   title: Option[Seq[String]],
                   birthday: Option[Date],
                   birthPlace: Option[String],
                   deathPlace: Option[String],
                   deathday: Option[Date],
                   prefName: Option[String],
                   gender: Option[String],
                   knwos: Option[Seq[String]], // link with Person
                   hasAlignment: Option[Seq[String]], // link with Person
                   isAuthorOf: Option[Seq[String]], // link with CollectionItem
                   isPublisherOf: Option[Seq[String]], // link with CollectionItem
                   isCreatorOf: Option[Seq[String]], // link with CollectionItem
                   isScientificEditorOf: Option[Seq[String]], // link with CollectionItem
                   idrefURI:  Option[String], // link with LOD
                   wikidataURI:  Option[String], // link with LOD
                   viafURI:  Option[String], // link with LOD
                   orcidURI:  Option[String], // link with LOD
                   bnfURI:  Option[String], // link with LOD
                   isniURI:  Option[String], // link with LOD
                   authoredTaxon:Option[Seq[String]] // taxon

                 ) extends Node {
  override def id: String = StringUtils.lastPathOfUri(entityId.get)
}

object Person {
  implicit object PersonId extends IdentifiableNode[EsContext, Person] {
    def id(ctx: Context[EsContext, Person]): String = ctx.value.id
  }

   val read1: Reads[
    (
      Option[String],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Date],
      Option[String],
      Option[String],
      Option[Date],
      Option[String],
      Option[String],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[String],
      Option[String],
      Option[String],
      Option[String]
      )
  ] = (
    (__ \ "entityId").readNullable[String] and
      (__ \ "lastName").readNullable[Seq[String]](readSeq) and
      (__ \ "firstName").readNullable[Seq[String]](readSeq) and
      (__ \ "nick").readNullable[Seq[String]](readSeq) and
      (__ \ "name").readNullable[Seq[String]](readSeq) and
      (__ \ "title").readNullable[Seq[String]](readSeq) and
      (__ \ "birthday").readNullable[Date] and
      (__ \ "birthPlace").readNullable[String] and
      (__ \ "deathPlace").readNullable[String] and
      (__ \ "deathday").readNullable[Date] and
      (__ \ "prefName").readNullable[String] and
      (__ \ "gender").readNullable[String] and
      (__ \ "knwos").readNullable[Seq[String]](readSeq) and
      (__ \ "hasAlignment").readNullable[Seq[String]](readSeq) and
      (__ \ "isAuthorOf").readNullable[Seq[String]](readSeq) and
      (__ \ "isPublisherOf").readNullable[Seq[String]](readSeq) and
      (__ \ "isCreatorOf").readNullable[Seq[String]](readSeq) and
      (__ \ "isScientificEditorOf").readNullable[Seq[String]](readSeq) and
      (__ \ "idrefURI").readNullable[String] and
      (__ \ "wikidataURI").readNullable[String] and
      (__ \ "viafURI").readNullable[String] and
      (__ \ "orcidURI").readNullable[String]
    ).tupled

  val read2: Reads[(
      Option[String],
      Option[String],
      Option[Seq[String]]
    )] = (
    (__ \ "bnfURI").readNullable[String] and
      (__ \ "isniURI").readNullable[String] and
      (__ \ "authoredTaxon").readNullable[Seq[String]](readSeq)
    ).tupled

  val f: (
    (
    Option[String],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Date],
      Option[String],
      Option[String],
      Option[Date],
      Option[String],
      Option[String],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[Seq[String]],
      Option[String],
      Option[String],
      Option[String],
      Option[String]
    ),
  (
    Option[String],
    Option[String],
    Option[Seq[String]]
    )
  ) => Person = {
    case (
      (entityId: Option[String],
      lastName: Option[Seq[String]],
      firstName: Option[Seq[String]],
      nick: Option[Seq[String]],
      name: Option[Seq[String]],
      title: Option[Seq[String]],
      birthday: Option[Date],

      birthPlace: Option[String],
      deathPlace: Option[String],
      deathday: Option[Date],
      prefName: Option[String],
      gender: Option[String],
      knwos: Option[Seq[String]], // link with Person
      hasAlignment: Option[Seq[String]], // link with Person
      isAuthorOf: Option[Seq[String]], // link with CollectionItem
      isPublisherOf: Option[Seq[String]], // link with CollectionItem
      isCreatorOf: Option[Seq[String]], // link with CollectionItem
      isScientificEditorOf: Option[Seq[String]], // link with CollectionItem
      idrefURI: Option[String], // link with LOD
      wikidataURI: Option[String], // link with LOD
      viafURI: Option[String], // link with LOD
      orcidURI: Option[String], // link with LOD)
      ),
      (
        bnfURI: Option[String], // link with LOD
        isniURI: Option[String], // link with LOD
        authoredTaxon: Option[Seq[String]] // taxon
        )
      ) => Person(
      entityId: Option[String],
      lastName: Option[Seq[String]],
      firstName: Option[Seq[String]],
      nick: Option[Seq[String]],
      name: Option[Seq[String]],
      title: Option[Seq[String]],
      birthday: Option[Date],
      birthPlace: Option[String],
      deathPlace: Option[String],
      deathday: Option[Date],
      prefName: Option[String],
      gender: Option[String],
      knwos: Option[Seq[String]], // link with Person
      hasAlignment: Option[Seq[String]], // link with Person
      isAuthorOf: Option[Seq[String]], // link with CollectionItem
      isPublisherOf: Option[Seq[String]], // link with CollectionItem
      isCreatorOf: Option[Seq[String]], // link with CollectionItem
      isScientificEditorOf: Option[Seq[String]], // link with CollectionItem
      idrefURI: Option[String], // link with LOD
      wikidataURI: Option[String], // link with LOD
      viafURI: Option[String], // link with LOD
      orcidURI: Option[String], // link with LOD
      bnfURI: Option[String], // link with LOD
      isniURI: Option[String], // link with LOD
      authoredTaxon: Option[Seq[String]] // taxon
    )
  }

  implicit val reads: Reads[Person] = (read1 and read2) {
    f
  }

  implicit val writes: Writes[Person] = new Writes[Person] {
    override def writes(o: Person): JsValue = Json.obj(
      "entityId" -> o.entityId,
      "lastName" -> o.lastName,
      "firstName" -> o.firstName,
      "nick" -> o.nick,
      "name" -> o.name,
      "title" -> o.title,
      "birthday" -> o.birthday.map(_.getTime), // Convert Date to Unix timestamp
      "birthPlace" -> o.birthPlace,
      "deathPlace" -> o.deathPlace,
      "deathday" -> o.deathday.map(_.getTime), // Convert Date to Unix timestamp
      "prefName" -> o.prefName,
      "gender" -> o.gender,
      "knwos" -> o.knwos,
      "hasAlignment" -> o.hasAlignment,
      "isAuthorOf" -> o.isAuthorOf,
      "isPublisherOf" -> o.isPublisherOf,
      "isCreatorOf" -> o.isCreatorOf,
      "isScientificEditorOf" -> o.isScientificEditorOf,
      "idrefURI" -> o.idrefURI,
      "wikidataURI" -> o.wikidataURI,
      "viafURI" -> o.viafURI,
      "orcidURI" -> o.orcidURI,
      "bnfURI" -> o.bnfURI,
      "isniURI" -> o.isniURI,
      "authoredTaxon" -> o.authoredTaxon
    )
  }

  implicit val ec: ExecutionContext = ExecutionContext.global
  import com.mnemotix.synaptix.api.graphql.helper.DateScalar._
/*
  val PersonType = deriveObjectType[Unit, Person](
    Interfaces(nodeInterface),
    ObjectTypeName("Person"),
    ObjectTypeDescription(s"""Information about a person. types = s"${nameSpaceMap.get("mnhn").get}Person"""),
    DocumentField("entityId", "The ID of the person."),
    DocumentField("lastName", "The last name(s) of the person."),
    DocumentField("firstName", "The first name(s) of the person."),
    DocumentField("nick", "The nickname(s) of the person."),
    DocumentField("name", "The name(s) of the person."),
    DocumentField("title", "The title(s) of the person."),
    DocumentField("birthday", "The birthday of the person."),
    DocumentField("birthPlace", "The birthplace of the person."),
    DocumentField("deathPlace", "The place of death of the person."),
    DocumentField("deathday", "The date of death of the person."),
    DocumentField("prefName", "The preferred name of the person."),
    DocumentField("gender", "The gender of the person."),
    DocumentField("knwos", "Links with other persons."),
    DocumentField("hasAlignment", "Links with other persons."),
    DocumentField("isAuthorOf", "Links with collection items authored by the person."),
    DocumentField("isPublisherOf", "Links with collection items published by the person."),
    DocumentField("isCreatorOf", "Links with collection items created by the person."),
    DocumentField("isScientificEditorOf", "Links with collection items edited by the person."),
    DocumentField("idrefURI", "Link with LOD (Linked Open Data) via IDREF."),
    DocumentField("wikidataURI", "Link with LOD (Linked Open Data) via Wikidata."),
    DocumentField("viafURI", "Link with LOD (Linked Open Data) via VIAF."),
    DocumentField("orcidURI", "Link with LOD (Linked Open Data) via ORCID."),
    DocumentField("bnfURI", "Link with LOD (Linked Open Data) via BNF."),
    DocumentField("isniURI", "Link with LOD (Linked Open Data) via ISNI."),
    ExcludeFields("authoredTaxon"),
    AddFields(
      Node.globalIdField,
      Field("isAuthorOfDocument",
        documentConnection,
        arguments = Connection.Args.All,
        resolve = ctx => {
        val uris = ctx.value.isAuthorOf.map(ur => ur.filter(_.contains("/document/")))
        DeferredValue(FetchersResolver.documentsFetcher.deferSeq(uris.toList.flatten)).map { fetched =>
          Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      ),
      Field(
        name = "taxon",
        fieldType =   taxonConnection,
        description = Some("Taxon(s) authored by the person."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.authoredTaxon
          DeferredValue(FetchersResolver.taxonFetcher.deferSeq(uris.toSeq.flatten)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      )
    )
  )

 */

  val PersonType: ObjectType[Unit, Person] = ObjectType(
    "Person",
    "Information about a person.",
    () => fields[Unit, Person](
      Node.globalIdField,
      Field("entityId", OptionType(StringType), Some("The ID of the person."), resolve = _.value.entityId),
      Field("lastName", OptionType(ListType(StringType)), Some("The last name(s) of the person."), resolve = _.value.lastName.getOrElse(Nil)),
      Field("firstName", OptionType(ListType(StringType)), Some("The first name(s) of the person."), resolve = _.value.firstName.getOrElse(Nil)),
      Field("nick", OptionType(ListType(StringType)), Some("The nickname(s) of the person."), resolve = _.value.nick.getOrElse(Nil)),
      Field("name", OptionType(ListType(StringType)), Some("The name(s) of the person."), resolve = _.value.name.getOrElse(Nil)),
      Field("title", OptionType(ListType(StringType)), Some("The title(s) of the person."), resolve = _.value.title.getOrElse(Nil)),
      Field("birthday", OptionType(DateType), Some("The birthday of the person."), resolve = _.value.birthday),
      Field("birthPlace", OptionType(StringType), Some("The birthplace of the person."), resolve = _.value.birthPlace),
      Field("deathPlace", OptionType(StringType), Some("The place of death of the person."), resolve = _.value.deathPlace),
      Field("deathday", OptionType(DateType), Some("The date of death of the person."), resolve = _.value.deathday),
      Field("prefName", OptionType(StringType), Some("The preferred name of the person."), resolve = _.value.prefName),
      Field("gender", OptionType(StringType), Some("The gender of the person."), resolve = _.value.gender),
      Field("knwos", OptionType(ListType(StringType)), Some("Links with other persons."), resolve = _.value.knwos.getOrElse(Nil)),
      Field("hasAlignment", OptionType(ListType(StringType)), Some("Links with other persons."), resolve = _.value.hasAlignment.getOrElse(Nil)),
      Field("isAuthorOf", OptionType(ListType(StringType)), Some("Links with collection items authored by the person."), resolve = _.value.isAuthorOf.getOrElse(Nil)),
      Field("isPublisherOf", OptionType(ListType(StringType)), Some("Links with collection items published by the person."), resolve = _.value.isPublisherOf.getOrElse(Nil)),
      Field("isCreatorOf", OptionType(ListType(StringType)), Some("Links with collection items created by the person."), resolve = _.value.isCreatorOf.getOrElse(Nil)),
      Field("isScientificEditorOf", OptionType(ListType(StringType)), Some("Links with collection items edited by the person."), resolve = _.value.isScientificEditorOf.getOrElse(Nil)),
      Field("idrefURI", OptionType(StringType), Some("Link with LOD (Linked Open Data) via IDREF."), resolve = _.value.idrefURI),
      Field("wikidataURI", OptionType(StringType), Some("Link with LOD (Linked Open Data) via Wikidata."), resolve = _.value.wikidataURI),
      Field("viafURI", OptionType(StringType), Some("Link with LOD (Linked Open Data) via VIAF."), resolve = _.value.viafURI),
      Field("orcidURI", OptionType(StringType), Some("Link with LOD (Linked Open Data) via ORCID."), resolve = _.value.orcidURI),
      Field("bnfURI", OptionType(StringType), Some("Link with LOD (Linked Open Data) via BNF."), resolve = _.value.bnfURI),
      Field("isniURI", OptionType(StringType), Some("Link with LOD (Linked Open Data) via ISNI."), resolve = _.value.isniURI),
      Field(
          name = "isAuthorOfDocument",
          fieldType = documentConnection,
          arguments = Connection.Args.All,
          resolve = ctx => {
            val uris = ctx.value.isAuthorOf.map(ur => ur.filter(_.contains("/document/")))
            DeferredValue(FetchersResolver.documentsFetcher.deferSeq(uris.toList.flatten)).map { fetched =>
              Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
            }
          }
        ),
        Field(
          name = "taxon",
          fieldType = taxonConnection,
          description = Some("Taxon(s) authored by the person."),
          arguments = Connection.Args.All,
          resolve = ctx => {
            val uris = ctx.value.authoredTaxon
            DeferredValue(FetchersResolver.taxonFetcher.deferSeq(uris.toSeq.flatten)).map { fetched =>
              Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
            }
          }
        )
    )
  )


  val ConnectionDefinition(_, personConnection) = Connection.definition[EsContext, Connection, Person](
    name = "Person",
    nodeType = PersonType
  )
}