package fr.mnhn.datahub.api.model

import fr.mnhn.datahub.api.utils.MnxJsonUtils.readSeq
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{Reads, __}
import sangria.macros.derive.{DocumentField, ObjectTypeDescription, deriveObjectType}
import sangria.relay.{Connection, ConnectionDefinition}
import sangria.schema.ObjectType

import scala.concurrent.ExecutionContext

case class ConceptRequestable(
                      altLabel: Option[Seq[String]],
                      broader: Option[Seq[String]],
                      broaderMatch: Option[Seq[String]],
                      broaderTransitive: Option[Seq[String]],
                      broaderTransitivePrefLabels: Option[Seq[String]],
                      changeNote: Option[Seq[String]],
                      closeMatch: Option[Seq[String]],
                      collection: Option[Seq[String]],
                      comment: Option[Seq[String]],
                      definition: Option[String],
                      entityId: Option[String],
                      exactMatch: Option[Seq[String]],
                      example: Option[String],

                      historyNote: Option[String],
                      inScheme: Option[Seq[String]],
                      narrowerMatch: Option[Seq[String]],
                      notation: Option[String],
                      note: Option[Seq[String]],
                      percoLabel: Option[String],
                      prefLabel: Option[Seq[String]],
                      related: Option[Seq[String]],
                      relatedMatch: Option[Seq[String]],
                      schemePrefLabel: Option[String],
                      scopeNote: Option[String],
                      seeAlso: Option[String],
                      topConceptOf: Option[Seq[String]],
                      types: Option[Seq[String]],
                      vocabularyPrefLabel: Option[String]
                    ) extends Concept

object ConceptRequestable {
  val read1:Reads[(Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]],
    Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]],
    Option[Seq[String]], Option[String], Option[String], Option[Seq[String]], Option[String])]  = (
    (__ \ "altLabel").readNullable[Seq[String]](readSeq) and
      (__ \ "broader").readNullable[Seq[String]](readSeq) and
      (__ \ "broaderMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "broaderTransitive").readNullable[Seq[String]](readSeq) and
      (__ \ "broaderTransitivePrefLabels").readNullable[Seq[String]](readSeq) and
      (__ \ "changeNote").readNullable[Seq[String]](readSeq) and
      (__ \ "closeMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "collection").readNullable[Seq[String]](readSeq) and
      (__ \ "comment").readNullable[Seq[String]](readSeq) and
      (__ \ "definition").readNullable[String] and
      (__ \ "entityId").readNullable[String] and
      (__ \ "exactMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "example").readNullable[String]
    ) .tupled

  val read2: Reads[
    (
      Option[String], // historyNote
        Option[Seq[String]], // inScheme
        Option[Seq[String]], // narrowerMatch
        Option[String], // notation
        Option[Seq[String]], // note
        Option[String], // percoLabel
        Option[Seq[String]], // prefLabel
        Option[Seq[String]], // related
        Option[Seq[String]], // relatedMatch
        Option[String], // schemePrefLabel
        Option[String], // scopeNote
        Option[String], // seeAlso
        Option[Seq[String]], // topConceptOf
        Option[Seq[String]], // types
        Option[String] // vocabularyPrefLabel
      )
  ] = (
    (__ \ "historyNote").readNullable[String] and
      (__ \ "inScheme").readNullable[Seq[String]](readSeq) and
      (__ \ "narrowerMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "notation").readNullable[String] and
      (__ \ "note").readNullable[Seq[String]](readSeq) and
      (__ \ "percoLabel").readNullable[String] and
      (__ \ "prefLabel").readNullable[Seq[String]](readSeq) and
      (__ \ "related").readNullable[Seq[String]](readSeq) and
      (__ \ "relatedMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "schemePrefLabel").readNullable[String] and
      (__ \ "scopeNote").readNullable[String] and
      (__ \ "seeAlso").readNullable[String] and
      (__ \ "topConceptOf").readNullable[Seq[String]](readSeq) and
      (__ \ "types").readNullable[Seq[String]](readSeq) and
      (__ \ "vocabularyPrefLabel").readNullable[String]
    ).tupled

  val f: (
    (Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]],
      Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]],
      Option[Seq[String]], Option[String], Option[String], Option[Seq[String]], Option[String]),
      (
        Option[String], // historyNote
          Option[Seq[String]], // inScheme
          Option[Seq[String]], // narrowerMatch
          Option[String], // notation
          Option[Seq[String]], // note
          Option[String], // percoLabel
          Option[Seq[String]], // prefLabel
          Option[Seq[String]], // related
          Option[Seq[String]], // relatedMatch
          Option[String], // schemePrefLabel
          Option[String], // scopeNote
          Option[String], // seeAlso
          Option[Seq[String]], // topConceptOf
          Option[Seq[String]], // types
          Option[String] // vocabularyPrefLabel
        )
    ) => ConceptRequestable = {
    case (
      (
        altLabel: Option[Seq[String]],
        broader: Option[Seq[String]],
        broaderMatch: Option[Seq[String]],
        broaderTransitive: Option[Seq[String]],
        broaderTransitivePrefLabels: Option[Seq[String]],
        changeNote: Option[Seq[String]],
        closeMatch: Option[Seq[String]],
        collection: Option[Seq[String]],
        comment: Option[Seq[String]],
        definition: Option[String],
        entityId: Option[String],
        exactMatch: Option[Seq[String]],
        example: Option[String]
        ),
      (
        historyNote: Option[String],
        inScheme: Option[Seq[String]],
        narrowerMatch: Option[Seq[String]],
        notation: Option[String],
        note: Option[Seq[String]],
        percoLabel: Option[String],
        prefLabel: Option[Seq[String]],
        related: Option[Seq[String]],
        relatedMatch: Option[Seq[String]],
        schemePrefLabel: Option[String],
        scopeNote: Option[String],
        seeAlso: Option[String],
        topConceptOf: Option[Seq[String]],
        types: Option[Seq[String]],
        vocabularyPrefLabel: Option[String]
        )
      ) => ConceptRequestable(
      altLabel: Option[Seq[String]],
      broader: Option[Seq[String]],
      broaderMatch: Option[Seq[String]],
      broaderTransitive: Option[Seq[String]],
      broaderTransitivePrefLabels: Option[Seq[String]],
      changeNote: Option[Seq[String]],
      closeMatch: Option[Seq[String]],
      collection: Option[Seq[String]],
      comment: Option[Seq[String]],
      definition: Option[String],
      entityId: Option[String],
      exactMatch: Option[Seq[String]],
      example: Option[String],

      historyNote: Option[String],
      inScheme: Option[Seq[String]],
      narrowerMatch: Option[Seq[String]],
      notation: Option[String],
      note: Option[Seq[String]],
      percoLabel: Option[String],
      prefLabel: Option[Seq[String]],
      related: Option[Seq[String]],
      relatedMatch: Option[Seq[String]],
      schemePrefLabel: Option[String],
      scopeNote: Option[String],
      seeAlso: Option[String],
      topConceptOf: Option[Seq[String]],
      types: Option[Seq[String]],
      vocabularyPrefLabel: Option[String]
    )
  }

  implicit val read: Reads[ConceptRequestable] = (read1 and read2) {
    f
  }

  implicit val ec: ExecutionContext = ExecutionContext.global

  val ConceptEsType: ObjectType[Unit, ConceptRequestable] = deriveObjectType[Unit, ConceptRequestable](
    ObjectTypeDescription("Concept"),
    DocumentField("altLabel", "Alternative labels for the concept."),
    DocumentField("broader", "Concepts that are broader in meaning than the current concept."),
    DocumentField("broaderMatch", "Concepts that are broader in meaning than the current concept, as a direct mapping."),
    DocumentField("broaderTransitive", "Concepts that are transitively broader in meaning than the current concept."),
    DocumentField("broaderTransitivePrefLabels", "Preferred labels for concepts that are transitively broader in meaning than the current concept."),
    DocumentField("changeNote", "Notes about changes to the concept over time."),
    DocumentField("closeMatch", "Concepts that are closely related to the current concept."),
    DocumentField("collection", "Collections that include the current concept."),
    DocumentField("comment", "Comments related to the concept."),
    DocumentField("definition", "Definition of the concept."),
    DocumentField("entityId", "The ID of the concept."),
    DocumentField("exactMatch", "Concepts that exactly match the current concept."),
    DocumentField("example", "An example demonstrating the concept."),
    DocumentField("historyNote", "Notes about the historical context of the concept."),
    DocumentField("inScheme", "Schemes to which the concept belongs."),
    DocumentField("narrowerMatch", "Concepts that are narrower in meaning than the current concept, as a direct mapping."),
    DocumentField("notation", "A notation representing the concept."),
    DocumentField("note", "Additional notes about the concept."),
    DocumentField("percoLabel", "Label used for percolation."),
    DocumentField("prefLabel", "Preferred labels for the concept."),
    DocumentField("related", "Concepts related to the current concept."),
    DocumentField("relatedMatch", "Concepts related to the current concept, as a direct mapping."),
    DocumentField("schemePrefLabel", "Preferred label for the scheme."),
    DocumentField("scopeNote", "Notes about the scope of the concept."),
    DocumentField("seeAlso", "Related resources or references."),
    DocumentField("topConceptOf", "Schemes for which the concept is a top concept."),
    DocumentField("types", "Types associated with the concept."),
    DocumentField("vocabularyPrefLabel", "Preferred label for the vocabulary.")
  )

  val ConnectionDefinition(_, conceptEsConnection) = Connection.definition[EsContext, Connection, ConceptRequestable](
    name = "Concept",
    nodeType = ConceptEsType
  )
}