package fr.mnhn.datahub.api.model

import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.api.DatahubSchema.nodeInterface
import fr.mnhn.datahub.api.model.ArchiveItem.archiveItemConnection
import fr.mnhn.datahub.api.model.CollectionItem.collectionItemConnection
import fr.mnhn.datahub.api.model.ConceptRequestable.conceptEsConnection
import fr.mnhn.datahub.api.model.Document.{DocumentType, documentConnection}
import fr.mnhn.datahub.api.resolvers.FetchersResolver
import fr.mnhn.datahub.api.utils.MnxJsonUtils.readSeq
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsValue, Json, Reads, Writes, __}
import sangria.macros.derive.{AddFields, DocumentField, ExcludeFields, IncludeMethods, Interfaces, ObjectTypeDescription, ReplaceField, deriveObjectType}
import sangria.relay.{Connection, ConnectionArgs, ConnectionDefinition, IdentifiableNode, Node}
import sangria.schema.{DeferredValue, Field, ObjectType}
import sangria.schema._

import scala.concurrent.ExecutionContext

case class TaxonName(
                      altLabel: Option[Seq[String]],
                      broader: Option[Seq[String]],
                      broaderMatch: Option[Seq[String]],
                      broaderTransitive: Option[Seq[String]],
                      broaderTransitivePrefLabels: Option[Seq[String]],
                      changeNote: Option[Seq[String]],
                      closeMatch: Option[Seq[String]],
                      collection: Option[Seq[String]],
                      comment: Option[Seq[String]],
                      definition: Option[String],
                      entityId: Option[String],
                      exactMatch: Option[Seq[String]],
                      example: Option[String],

                      historyNote: Option[String],
                      inScheme: Option[Seq[String]],
                      narrowerMatch: Option[Seq[String]],
                      notation: Option[String],
                      note: Option[Seq[String]],
                      percoLabel: Option[String],
                      prefLabel: Option[Seq[String]],
                      related: Option[Seq[String]],
                      relatedMatch: Option[Seq[String]],
                      schemePrefLabel: Option[String],
                      scopeNote: Option[String],
                      seeAlso: Option[String],
                      topConceptOf: Option[Seq[String]],
                      types: Option[Seq[String]],
                      vocabularyPrefLabel: Option[String],

                      hasAuthority: Option[String],
                      scientificName: Option[String],
                      identifier: Option[String],
                      namePublishedIn: Option[Seq[String]],
                      taxonID: Option[String],
                      taxonRank: Option[String]
                    ) extends Concept with Node {
  override def id: String =(entityId.get)
}

object TaxonName {
  import sangria.schema._

  implicit val ec: ExecutionContext = ExecutionContext.global

  val read1:Reads[(Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]],
    Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]],
    Option[Seq[String]], Option[String], Option[String], Option[Seq[String]], Option[String])]  = (
    (__ \ "altLabel").readNullable[Seq[String]](readSeq) and
      (__ \ "broader").readNullable[Seq[String]](readSeq) and
      (__ \ "broaderMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "broaderTransitive").readNullable[Seq[String]](readSeq) and
      (__ \ "broaderTransitivePrefLabels").readNullable[Seq[String]](readSeq) and
      (__ \ "changeNote").readNullable[Seq[String]](readSeq) and
      (__ \ "closeMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "collection").readNullable[Seq[String]](readSeq) and
      (__ \ "comment").readNullable[Seq[String]](readSeq) and
      (__ \ "definition").readNullable[String] and
      (__ \ "entityId").readNullable[String] and
      (__ \ "exactMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "example").readNullable[String]
  ) .tupled

  val read2: Reads[
    (
      Option[String],      // historyNote
        Option[Seq[String]], // inScheme
        Option[Seq[String]], // narrowerMatch
        Option[String],      // notation
        Option[Seq[String]], // note
        Option[String],      // percoLabel
        Option[Seq[String]], // prefLabel
        Option[Seq[String]], // related
        Option[Seq[String]], // relatedMatch
        Option[String],      // schemePrefLabel
        Option[String],      // scopeNote
        Option[String],      // seeAlso
        Option[Seq[String]], // topConceptOf
        Option[Seq[String]], // types
        Option[String]       // vocabularyPrefLabel
      )
  ] = (
    (__ \ "historyNote").readNullable[String] and
      (__ \ "inScheme").readNullable[Seq[String]](readSeq) and
      (__ \ "narrowerMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "notation").readNullable[String] and
      (__ \ "note").readNullable[Seq[String]](readSeq) and
      (__ \ "percoLabel").readNullable[String] and
      (__ \ "prefLabel").readNullable[Seq[String]](readSeq) and
      (__ \ "related").readNullable[Seq[String]](readSeq) and
      (__ \ "relatedMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "schemePrefLabel").readNullable[String] and
      (__ \ "scopeNote").readNullable[String] and
      (__ \ "seeAlso").readNullable[String] and
      (__ \ "topConceptOf").readNullable[Seq[String]](readSeq) and
      (__ \ "types").readNullable[Seq[String]](readSeq) and
      (__ \ "vocabularyPrefLabel").readNullable[String]
    ).tupled

  val read3: Reads[
    (
      Option[String],      // hasAuthority
        Option[String],      // scientificName
        Option[String],      // identifier
        Option[Seq[String]],  // namePublishedIn
        Option[String],      // scientificName
        Option[String]
      )
  ] = (
    (__ \ "hasAuthority").readNullable[String] and
      (__ \ "scientificName").readNullable[String] and
      (__ \ "identifier").readNullable[String] and
      (__ \ "namePublishedIn").readNullable[Seq[String]](readSeq) and
      (__ \ "taxonID").readNullable[String] and
      (__ \ "taxonRank").readNullable[String]
    ).tupled

  val f: (
    (Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]],
      Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]],
      Option[Seq[String]], Option[String], Option[String], Option[Seq[String]], Option[String]),
      (
        Option[String],      // historyNote
          Option[Seq[String]], // inScheme
          Option[Seq[String]], // narrowerMatch
          Option[String],      // notation
          Option[Seq[String]], // note
          Option[String],      // percoLabel
          Option[Seq[String]], // prefLabel
          Option[Seq[String]], // related
          Option[Seq[String]], // relatedMatch
          Option[String],      // schemePrefLabel
          Option[String],      // scopeNote
          Option[String],      // seeAlso
          Option[Seq[String]], // topConceptOf
          Option[Seq[String]], // types
          Option[String]       // vocabularyPrefLabel
        ),
      (
        Option[String],      // hasAuthority
          Option[String],      // scientificName
          Option[String],      // identifier
          Option[Seq[String]],  // namePublishedIn
          Option[String],
          Option[String]
        )
    ) => TaxonName = {
    case (
      (
        altLabel: Option[Seq[String]],
        broader: Option[Seq[String]],
        broaderMatch: Option[Seq[String]],
        broaderTransitive: Option[Seq[String]],
        broaderTransitivePrefLabels: Option[Seq[String]],
        changeNote: Option[Seq[String]],
        closeMatch: Option[Seq[String]],
        collection: Option[Seq[String]],
        comment: Option[Seq[String]],
        definition: Option[String],
        entityId: Option[String],
        exactMatch: Option[Seq[String]],
        example: Option[String]
      ),
      (
        historyNote: Option[String],
        inScheme: Option[Seq[String]],
        narrowerMatch: Option[Seq[String]],
        notation: Option[String],
        note: Option[Seq[String]],
        percoLabel: Option[String],
        prefLabel: Option[Seq[String]],
        related: Option[Seq[String]],
        relatedMatch: Option[Seq[String]],
        schemePrefLabel: Option[String],
        scopeNote: Option[String],
        seeAlso: Option[String],
        topConceptOf: Option[Seq[String]],
        types: Option[Seq[String]],
        vocabularyPrefLabel: Option[String]
      ),
      (
        hasAuthority: Option[String],
        scientificName: Option[String],
        identifier: Option[String],
        namePublishedIn: Option[Seq[String]],
        taxonID: Option[String],
        taxonRank: Option[String]
      )
    ) => TaxonName(
      altLabel: Option[Seq[String]],
      broader: Option[Seq[String]],
      broaderMatch: Option[Seq[String]],
      broaderTransitive: Option[Seq[String]],
      broaderTransitivePrefLabels: Option[Seq[String]],
      changeNote: Option[Seq[String]],
      closeMatch: Option[Seq[String]],
      collection: Option[Seq[String]],
      comment: Option[Seq[String]],
      definition: Option[String],
      entityId: Option[String],
      exactMatch: Option[Seq[String]],
      example: Option[String],

      historyNote: Option[String],
      inScheme: Option[Seq[String]],
      narrowerMatch: Option[Seq[String]],
      notation: Option[String],
      note: Option[Seq[String]],
      percoLabel: Option[String],
      prefLabel: Option[Seq[String]],
      related: Option[Seq[String]],
      relatedMatch: Option[Seq[String]],
      schemePrefLabel: Option[String],
      scopeNote: Option[String],
      seeAlso: Option[String],
      topConceptOf: Option[Seq[String]],
      types: Option[Seq[String]],
      vocabularyPrefLabel: Option[String],

      hasAuthority: Option[String],
      scientificName: Option[String],
      identifier: Option[String],
      namePublishedIn: Option[Seq[String]],
      taxonID: Option[String],
      taxonRank: Option[String]
    )
  }

  implicit val read: Reads[TaxonName] = (read1 and read2 and read3) {
    f
  }

  implicit val writes: Writes[TaxonName] = new Writes[TaxonName] {
    override def writes(o: TaxonName): JsValue = Json.obj(
      "altLabel" -> o.altLabel,
      "broader" -> o.broader,
      "broaderMatch" -> o.broaderMatch,
      "broaderTransitive" -> o.broaderTransitive,
      "broaderTransitivePrefLabels" -> o.broaderTransitivePrefLabels,
      "changeNote" -> o.changeNote,
      "closeMatch" -> o.closeMatch,
      "collection" -> o.collection,
      "comment" -> o.comment,
      "definition" -> o.definition,
      "entityId" -> o.entityId,
      "exactMatch" -> o.exactMatch,
      "example" -> o.example,
      "historyNote" -> o.historyNote,
      "inScheme" -> o.inScheme,
      "narrowerMatch" -> o.narrowerMatch,
      "notation" -> o.notation,
      "note" -> o.note,
      "percoLabel" -> o.percoLabel,
      "prefLabel" -> o.prefLabel,
      "related" -> o.related,
      "relatedMatch" -> o.relatedMatch,
      "schemePrefLabel" -> o.schemePrefLabel,
      "scopeNote" -> o.scopeNote,
      "seeAlso" -> o.seeAlso,
      "topConceptOf" -> o.topConceptOf,
      "types" -> o.types,
      "vocabularyPrefLabel" -> o.vocabularyPrefLabel,
      "hasAuthority" -> o.hasAuthority,
      "scientificName" -> o.scientificName,
      "identifier" -> o.identifier,
      "namePublishedIn" -> o.namePublishedIn
    )
  }


    implicit object TaxonNameId extends IdentifiableNode[EsContext, TaxonName] {
    def id(ctx: Context[EsContext, TaxonName]): String = ctx.value.id
  }


  val TaxonNameType: ObjectType[Unit, TaxonName] =
    deriveObjectType[Unit, TaxonName](
//      Interfaces(nodeInterface),
      ObjectTypeDescription("TaxonName"),
      DocumentField("entityId", "The ID of the taxon name."),
      DocumentField("altLabel", "Alternative labels for the taxon name."),
      DocumentField("broader", "Taxon names that are broader in meaning than the current taxon name."),
      DocumentField("broaderMatch", "Taxon names that are broader in meaning than the current taxon name, as a direct mapping."),
      DocumentField("broaderTransitive", "Taxon names that are transitively broader in meaning than the current taxon name."),
      DocumentField("broaderTransitivePrefLabels", "Preferred labels for taxon names that are transitively broader in meaning than the current taxon name."),
      DocumentField("changeNote", "Notes about changes to the taxon name over time."),
      DocumentField("closeMatch", "Taxon names that are closely related to the current taxon name."),
      DocumentField("collection", "Collections that include the current taxon name."),
      DocumentField("comment", "Comments related to the taxon name."),
      DocumentField("definition", "Definition of the taxon name."),
      DocumentField("exactMatch", "Taxon names that exactly match the current taxon name."),
      DocumentField("example", "An example demonstrating the taxon name."),
      DocumentField("historyNote", "Notes about the historical context of the taxon name."),
      DocumentField("inScheme", "Schemes to which the taxon name belongs."),
      DocumentField("narrowerMatch", "Taxon names that are narrower in meaning than the current taxon name, as a direct mapping."),
      DocumentField("notation", "A notation representing the taxon name."),
      DocumentField("note", "Additional notes about the taxon name."),
      DocumentField("percoLabel", "Label used for percolation."),
      DocumentField("prefLabel", "Preferred labels for the taxon name."),
      DocumentField("related", "Taxon names related to the current taxon name."),
      DocumentField("relatedMatch", "Taxon names related to the current taxon name, as a direct mapping."),
      DocumentField("schemePrefLabel", "Preferred label for the scheme."),
      DocumentField("scopeNote", "Notes about the scope of the taxon name."),
      DocumentField("seeAlso", "Related resources or references."),
      DocumentField("topConceptOf", "Schemes for which the taxon name is a top concept."),
      DocumentField("types", "Types associated with the taxon name."),
      DocumentField("vocabularyPrefLabel", "Preferred label for the vocabulary."),
      DocumentField("hasAuthority", "The authority responsible for the taxon name."),
      DocumentField("scientificName", "The scientific name of the taxon."),
      DocumentField("identifier", "Identifier of the taxon name."),
      DocumentField("taxonID", "The taxon ID."),
      DocumentField("taxonRank", "The taxon rank."),
      ExcludeFields("namePublishedIn"),
      AddFields(Node.globalIdField,
        Field(
          name = "document",
          fieldType = documentConnection,
          description = Some("References to publications in which the taxon name was published."),
          arguments = Connection.Args.All,
          resolve = ctx => {
            val uris = ctx.value.namePublishedIn.getOrElse(Nil)
            val urisDocument = uris.filter(uri => uri.contains("Document"))
            DeferredValue(FetchersResolver.documentsFetcher.deferSeq(urisDocument)).map { fetched =>
              Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
            }
          }
        )
      ),
      /*
      ReplaceField(
        fieldName = "namePublishedIn",
        field = Field(
          name = "namePublishedIn",
          fieldType = documentConnection,
          description = Some("References to publications in which the taxon name was published."),
          arguments = Connection.Args.All,
          resolve = ctx => {
            val uris = ctx.value.namePublishedIn.getOrElse(Nil)
            val urisDocument = uris.filter(uri => uri.contains("Document"))
            DeferredValue(FetchersResolver.documentsFetcher.deferSeq(urisDocument)).map { fetched =>
              Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
            }
          }
        )
      )
      */
    )

  val ConnectionDefinition(_, taxonNameConnection) = Connection.definition[EsContext, Connection, TaxonName](
      name = "TaxonName",
      nodeType = TaxonNameType
  )
}