package fr.mnhn.datahub.api.model

import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.api.DatahubSchema.nodeInterface
import fr.mnhn.datahub.api.model.TaxonName.taxonNameConnection
import fr.mnhn.datahub.api.resolvers.FetchersResolver
import play.api.libs.json.Json
import sangria.macros.derive.{AddFields, DocumentField, ExcludeFields, Interfaces, ObjectTypeDescription, ObjectTypeName, deriveObjectType}
import sangria.relay.{Connection, ConnectionArgs, ConnectionDefinition, IdentifiableNode, Node}
import sangria.schema.{Context, DeferredValue, Field, ObjectType}

import scala.concurrent.ExecutionContext

case class TaxonAlignment(entityId: String, confidenceScore: Double, taxonAlignedTo: Option[String] //, isTaxonAlignmentOf: String
                         )
/*
  extends Node {
  override def id: String = StringUtils.lastPathOfUri(entityId)
}
*/

object TaxonAlignment {
  implicit lazy val format = Json.format[TaxonAlignment]
  implicit val ec: ExecutionContext = ExecutionContext.global

  /*

  implicit object TaxonAlignmentId extends IdentifiableNode[EsContext, TaxonAlignment] {
    def id(ctx: Context[EsContext, TaxonAlignment]): String = ctx.value.id
  }
   */

  val TaxonAlignmentType: ObjectType[Unit, TaxonAlignment] = deriveObjectType(
    ObjectTypeName("TaxonAlignment"),
    ObjectTypeDescription("A type representing taxon alignment information."),
    DocumentField("entityId", "The ID of the taxon alignment."),
    DocumentField("confidenceScore",  ("The confidence score of the taxon alignment.")),
    ExcludeFields("taxonAlignedTo"), //, "The taxon aligned to."
    AddFields(
      Field(
        name = "taxon",
        fieldType = taxonNameConnection,
        description = Some("The taxon aligned to."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.taxonAlignedTo
          DeferredValue(FetchersResolver.taxonNameFetcher.deferSeq(uris.toList)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      )
    )
  )

  val ConnectionDefinition(_, taxonAlignmentConnection) = Connection.definition[EsContext, Connection, TaxonAlignment](
    name = "TaxonAlignment",
    nodeType = TaxonAlignmentType
  )
}