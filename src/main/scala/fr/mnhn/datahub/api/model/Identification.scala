/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api.model

import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.api.model.Material.materialConnection
import fr.mnhn.datahub.api.model.Person.personConnection
import fr.mnhn.datahub.api.model.Taxon.taxonConnection
import fr.mnhn.datahub.api.resolvers.FetchersResolver
import fr.mnhn.datahub.api.utils.MnxJsonUtils.readSeq
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsValue, Json, Reads, Writes, __}
import sangria.macros.derive.{AddFields, DocumentField, ExcludeFields, Interfaces, ObjectTypeDescription, ObjectTypeName, ReplaceField, deriveObjectType}
import sangria.relay.{Connection, ConnectionArgs, ConnectionDefinition, IdentifiableNode, Node}
import sangria.schema.{Context, DeferredValue, Field, ObjectType, OptionType, StringType, fields, IntType}

import java.util.Date
import scala.concurrent.ExecutionContext

case class Identification(
                           entityId: Option[String],
                           identificationID: Option[String],
                           identificationOrder: Option[Int],
                           dateIdentified: Option[Date],
                           typeStatus: Option[String],
                           identificationQualifier: Option[String],
                           identificationRemarks: Option[String],
                           typeStatusRemarks: Option[String],
                           verbatimDateIdentified: Option[String],
                           identifiedBy: Option[Seq[String]], // Assuming this is a URI represented as a String
                           hasMaterial: Option[Seq[String]], // Assuming this is a URI represented as a String
                           toTaxon: Option[Seq[String]] // Assuming this is a URI represented as a String
                         ) extends Node {
  override def id: String = StringUtils.lastPathOfUri(entityId.get)
}

object Identification {
  implicit val identificationReads: Reads[Identification] = (
    (__ \ "entityId").readNullable[String] and
      (__ \ "identificationID").readNullable[String] and
      (__ \ "identificationOrder").readNullable[Int] and
      (__ \ "dateIdentified").readNullable[Date] and
      (__ \ "typeStatus").readNullable[String] and
      (__ \ "identificationQualifier").readNullable[String] and
      (__ \ "identificationRemarks").readNullable[String] and
      (__ \ "typeStatusRemarks").readNullable[String] and
      (__ \ "verbatimDateIdentified").readNullable[String] and
      (__ \ "identifiedBy").readNullable[Seq[String]](readSeq) and
      (__ \ "hasMaterial").readNullable[Seq[String]](readSeq) and
      (__ \ "toTaxon").readNullable[Seq[String]](readSeq)
    )(Identification.apply _)

  implicit val identificationWrites: Writes[Identification] = new Writes[Identification] {
    override def writes(identification: Identification): JsValue = Json.obj(
      "entityId" -> identification.entityId,
      "identificationID" -> identification.identificationID,
      "identificationOrder" -> identification.identificationOrder,
      "dateIdentified" -> identification.dateIdentified.map(_.getTime),
      "typeStatus" -> identification.typeStatus,
      "identificationQualifier" -> identification.identificationQualifier,
      "identificationRemarks" -> identification.identificationRemarks,
      "typeStatusRemarks" -> identification.typeStatusRemarks,
      "verbatimDateIdentified" -> identification.verbatimDateIdentified,
      "identifiedBy" -> identification.identifiedBy,
      "hasMaterial" -> identification.hasMaterial,
      "toTaxon" -> identification.toTaxon
    )
  }

  implicit val ec: ExecutionContext = ExecutionContext.global
  import com.mnemotix.synaptix.api.graphql.helper.DateScalar._


  implicit object IdentificationId extends IdentifiableNode[EsContext, Identification] {
    def id(ctx: Context[EsContext, Identification]): String = ctx.value.id
  }

  val IdentificationType: ObjectType[EsContext, Identification] =   ObjectType(
    "Identification",
    "An identification of a taxon.",
    () => fields[EsContext, Identification](
      Node.globalIdField,
      Field("entityId", OptionType(StringType), Some("The ID of the identification."), resolve = _.value.entityId),
      Field("identificationID", OptionType(StringType), Some("The identification ID."), resolve = _.value.identificationID),
      Field("identificationOrder", OptionType(IntType), Some("The order of the identification."), resolve = _.value.identificationOrder),
      Field("dateIdentified", OptionType(DateType), Some("The date when the taxon was identified."), resolve = _.value.dateIdentified),
      Field("typeStatus", OptionType(StringType), Some("The type status of the taxon."), resolve = _.value.typeStatus),
      Field("identificationQualifier", OptionType(StringType), Some("The qualifier for the identification."), resolve = _.value.identificationQualifier),
      Field("identificationRemarks", OptionType(StringType), Some("Remarks about the identification."), resolve = _.value.identificationRemarks),
      Field("typeStatusRemarks", OptionType(StringType), Some("Remarks about the type status."), resolve = _.value.typeStatusRemarks),
      Field("verbatimDateIdentified", OptionType(StringType), Some("The verbatim date when the taxon was identified."), resolve = _.value.verbatimDateIdentified),

      Field(
          name = "identifiedBy",
          fieldType = personConnection,
          description = Some("The individuals who made the identification."),
          arguments = Connection.Args.All,
          resolve = ctx => {
            val uris = ctx.value.identifiedBy.getOrElse(Nil)
            DeferredValue(FetchersResolver.personsFetcher.deferSeq(uris)).map { fetched =>
              Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
            }
          }
        ),
        Field(
          name = "material",
          fieldType = materialConnection,
          description = Some("The material associated with the identification."),
          arguments = Connection.Args.All,
          resolve = ctx => {
            val uris = ctx.value.hasMaterial.getOrElse(Nil)
            DeferredValue(FetchersResolver.materialFetcher.deferSeq(uris)).map { fetched =>
              Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
            }
          }
        ),
        Field(
          name = "taxon",
          fieldType = taxonConnection,
          description = Some("The taxon to which the identification refers."),
          arguments = Connection.Args.All,
          resolve = ctx => {
            val uris = ctx.value.toTaxon.getOrElse(Nil)
            DeferredValue(FetchersResolver.taxonFetcher.deferSeq(uris)).map { fetched =>
              Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
            }
          }
        )
      )
  )

  val ConnectionDefinition(_, identificationConnection) = Connection.definition[EsContext, Connection, Identification](
    name = "Identification",
    nodeType = IdentificationType
  )

}