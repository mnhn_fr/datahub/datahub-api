/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api.model

import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.api.model.Geometry.geometryConnection
import fr.mnhn.datahub.api.resolvers.FetchersResolver
import play.api.libs.json.Json
import sangria.macros.derive.{AddFields, DocumentField, ExcludeFields, Interfaces, ObjectTypeDescription, ObjectTypeName, ReplaceField, deriveObjectType}
import sangria.relay.{Connection, ConnectionArgs, ConnectionDefinition, IdentifiableNode, Node}
import sangria.schema.{Context, DeferredValue, Field, ObjectType}

import scala.concurrent.ExecutionContext

case class Geolocation(entityId: Option[String],
                       locationID: Option[String],
                       continent: Option[String],
                       country: Option[String],
                       countryCode: Option[String],
                       municipality: Option[String],
                       locality: Option[String],
                       sea: Option[String],
                       ocean: Option[String],
                       locationRemarks: Option[String],
                       islandGroup: Option[String],
                       island: Option[String],
                       originalName: Option[String],
                       verbatimCountry: Option[String],
                       firstAdministrativeLevel: Option[String],
                       secondAdministrativeLevel: Option[String],
                       hasGeometry: Option[String],
                       hasGeolocation: Option[String],
                       recordedBy: Option[String]
                      ) extends Node {
  override def id: String = StringUtils.lastPathOfUri(entityId.get)

}

object Geolocation {
  implicit lazy val format = Json.format[Geolocation]

  implicit val ec: ExecutionContext = ExecutionContext.global


    implicit object GeolocationId extends IdentifiableNode[EsContext, Geolocation] {
    def id(ctx: Context[EsContext, Geolocation]): String = ctx.value.id
  }


  val GeolocationType: ObjectType[EsContext, Geolocation] = deriveObjectType[EsContext, Geolocation](
    ObjectTypeName("Geolocation"),
    ObjectTypeDescription("Geolocation information."),
   // Interfaces(nodeInterface),
    DocumentField("entityId", "The ID of the geolocation."),
    DocumentField("locationID", "The location ID."),
    DocumentField("continent", "The continent."),
    DocumentField("country", "The country."),
    DocumentField("countryCode", "The country code."),
    DocumentField("municipality", "The municipality."),
    DocumentField("locality", "The locality."),
    DocumentField("sea", "The sea."),
    DocumentField("ocean", "The ocean."),
    DocumentField("locationRemarks", "Remarks about the location."),
    DocumentField("islandGroup", "The island group."),
    DocumentField("island", "The island."),
    DocumentField("originalName", "The original name of the location."),
    DocumentField("verbatimCountry", "The verbatim country."),
    DocumentField("firstAdministrativeLevel", "The first administrative level."),
    DocumentField("secondAdministrativeLevel", "The second administrative level."),
    DocumentField("hasGeolocation", "Information about whether the location has geolocation."),
    DocumentField("recordedBy", "The person who recorded the geolocation."),
    ExcludeFields("hasGeometry"),
    AddFields(Node.globalIdField,
      Field(
        name = "geometry",
        fieldType = geometryConnection,
        description = Some("Information about whether the location has geometry."),
        arguments = Connection.Args.All,
        resolve = ctx => {
          val uris = ctx.value.hasGeometry
          DeferredValue(FetchersResolver.geometryFetcher.deferSeq(uris.toSeq)).map { fetched =>
            Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
          }
        }
      )
    )
  )

  val ConnectionDefinition(_, geolocationConnection) = Connection.definition[EsContext, Connection, Geolocation](
    name = "Geolocation",
    nodeType = GeolocationType
  )
}