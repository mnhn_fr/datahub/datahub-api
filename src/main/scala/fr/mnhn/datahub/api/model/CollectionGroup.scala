package fr.mnhn.datahub.api.model

import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.api.DatahubSchema.nodeInterface
import play.api.libs.json.Json
import sangria.introspection.TypeKind.Interface
import sangria.macros.derive.{AddFields, DocumentField, Interfaces, ObjectTypeDescription, ObjectTypeName, deriveObjectType}
import sangria.relay.{Connection, ConnectionDefinition, IdentifiableNode, Node}
import sangria.schema.{Context, ObjectType}

case class CollectionGroup(
                            entityId: Option[String],
                            label: Option[String],
                            description: Option[String]
                          ) extends Node {
  override def id: String = StringUtils.lastPathOfUri(entityId.get)
}

object CollectionGroup {
  implicit lazy val format = Json.format[CollectionGroup]

  implicit object CollectionGroupId extends IdentifiableNode[EsContext, CollectionGroup] {
    def id(ctx: Context[EsContext, CollectionGroup]): String = ctx.value.id
  }

  val CollectionGroupType: ObjectType[EsContext, CollectionGroup] = deriveObjectType[EsContext, CollectionGroup](
//    Interfaces(nodeInterface),
    ObjectTypeName("CollectionGroup"),
    ObjectTypeDescription("Collection Group information."),
    DocumentField("entityId", "The ID of the collection."),
    DocumentField("label", "The label of the Collection Group."),
    DocumentField("description", "Description of the Collection Group."),
    AddFields(Node.globalIdField),

  )

  val ConnectionDefinition(_, collectionGroupConnection) = Connection.definition[EsContext, Connection, CollectionGroup](
    name = "CollectionGroup",
    nodeType = CollectionGroupType
  )
}