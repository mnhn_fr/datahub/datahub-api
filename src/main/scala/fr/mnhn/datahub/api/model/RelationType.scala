package fr.mnhn.datahub.api.model

import com.mnemotix.synaptix.core.utils.StringUtils
import fr.mnhn.datahub.api.DatahubSchema.nodeInterface
import fr.mnhn.datahub.api.model.Material.materialConnection
import fr.mnhn.datahub.api.resolvers.FetchersResolver
import fr.mnhn.datahub.api.utils.MnxJsonUtils.readSeq
import play.api.libs.functional.syntax.toFunctionalBuilderOps
import play.api.libs.json.{JsValue, Json, Reads, Writes, __}
import sangria.macros.derive.{AddFields, DocumentField, Interfaces, ObjectTypeDescription, ObjectTypeName, ReplaceField, deriveObjectType}
import sangria.relay.{Connection, ConnectionArgs, ConnectionDefinition, IdentifiableNode, Node}
import sangria.schema.{DeferredValue, Field, ObjectType}
import sangria.schema._

import scala.concurrent.ExecutionContext

case class RelationType(altLabel: Option[Seq[String]],
                        broader: Option[Seq[String]],
                        broaderMatch: Option[Seq[String]],
                        broaderTransitive: Option[Seq[String]],
                        broaderTransitivePrefLabels: Option[Seq[String]],
                        changeNote: Option[Seq[String]],
                        closeMatch: Option[Seq[String]],
                        collection: Option[Seq[String]],
                        comment: Option[Seq[String]],
                        definition: Option[String],
                        entityId: Option[String],
                        exactMatch: Option[Seq[String]],
                        example: Option[String],

                        historyNote: Option[String],
                        inScheme: Option[Seq[String]],
                        narrowerMatch: Option[Seq[String]],
                        notation: Option[String],
                        note: Option[Seq[String]],
                        percoLabel: Option[String],
                        prefLabel: Option[Seq[String]],
                        related: Option[Seq[String]],
                        relatedMatch: Option[Seq[String]],
                        schemePrefLabel: Option[String],
                        scopeNote: Option[String],
                        seeAlso: Option[String],
                        topConceptOf: Option[Seq[String]],
                        types: Option[Seq[String]],
                        vocabularyPrefLabel: Option[String]
                       ) extends Concept
/* with Node {
  override def id: String = StringUtils.lastPathOfUri(entityId.get)

}
*/


object RelationType {
  import sangria.schema._

  val read1:Reads[(Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]],
    Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]],
    Option[Seq[String]], Option[String], Option[String], Option[Seq[String]], Option[String])]  = (
    (__ \ "altLabel").readNullable[Seq[String]](readSeq) and
      (__ \ "broader").readNullable[Seq[String]](readSeq) and
      (__ \ "broaderMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "broaderTransitive").readNullable[Seq[String]](readSeq) and
      (__ \ "broaderTransitivePrefLabels").readNullable[Seq[String]](readSeq) and
      (__ \ "changeNote").readNullable[Seq[String]](readSeq) and
      (__ \ "closeMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "collection").readNullable[Seq[String]](readSeq) and
      (__ \ "comment").readNullable[Seq[String]](readSeq) and
      (__ \ "definition").readNullable[String] and
      (__ \ "entityId").readNullable[String] and
      (__ \ "exactMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "example").readNullable[String]
    ) .tupled

  val read2: Reads[
    (
      Option[String],      // historyNote
        Option[Seq[String]], // inScheme
        Option[Seq[String]], // narrowerMatch
        Option[String],      // notation
        Option[Seq[String]], // note
        Option[String],      // percoLabel
        Option[Seq[String]], // prefLabel
        Option[Seq[String]], // related
        Option[Seq[String]], // relatedMatch
        Option[String],      // schemePrefLabel
        Option[String],      // scopeNote
        Option[String],      // seeAlso
        Option[Seq[String]], // topConceptOf
        Option[Seq[String]], // types
        Option[String]       // vocabularyPrefLabel
      )
  ] = (
    (__ \ "historyNote").readNullable[String] and
      (__ \ "inScheme").readNullable[Seq[String]](readSeq) and
      (__ \ "narrowerMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "notation").readNullable[String] and
      (__ \ "note").readNullable[Seq[String]](readSeq) and
      (__ \ "percoLabel").readNullable[String] and
      (__ \ "prefLabel").readNullable[Seq[String]](readSeq) and
      (__ \ "related").readNullable[Seq[String]](readSeq) and
      (__ \ "relatedMatch").readNullable[Seq[String]](readSeq) and
      (__ \ "schemePrefLabel").readNullable[String] and
      (__ \ "scopeNote").readNullable[String] and
      (__ \ "seeAlso").readNullable[String] and
      (__ \ "topConceptOf").readNullable[Seq[String]](readSeq) and
      (__ \ "types").readNullable[Seq[String]](readSeq) and
      (__ \ "vocabularyPrefLabel").readNullable[String]
    ).tupled


  val f: (
    (Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]],
      Option[Seq[String]], Option[Seq[String]], Option[Seq[String]], Option[Seq[String]],
      Option[Seq[String]], Option[String], Option[String], Option[Seq[String]], Option[String]),
      (
        Option[String],      // historyNote
          Option[Seq[String]], // inScheme
          Option[Seq[String]], // narrowerMatch
          Option[String],      // notation
          Option[Seq[String]], // note
          Option[String],      // percoLabel
          Option[Seq[String]], // prefLabel
          Option[Seq[String]], // related
          Option[Seq[String]], // relatedMatch
          Option[String],      // schemePrefLabel
          Option[String],      // scopeNote
          Option[String],      // seeAlso
          Option[Seq[String]], // topConceptOf
          Option[Seq[String]], // types
          Option[String]       // vocabularyPrefLabel
        )

    ) => RelationType = {
    case (
      (
        altLabel: Option[Seq[String]],
        broader: Option[Seq[String]],
        broaderMatch: Option[Seq[String]],
        broaderTransitive: Option[Seq[String]],
        broaderTransitivePrefLabels: Option[Seq[String]],
        changeNote: Option[Seq[String]],
        closeMatch: Option[Seq[String]],
        collection: Option[Seq[String]],
        comment: Option[Seq[String]],
        definition: Option[String],
        entityId: Option[String],
        exactMatch: Option[Seq[String]],
        example: Option[String]
        ),
      (
        historyNote: Option[String],
        inScheme: Option[Seq[String]],
        narrowerMatch: Option[Seq[String]],
        notation: Option[String],
        note: Option[Seq[String]],
        percoLabel: Option[String],
        prefLabel: Option[Seq[String]],
        related: Option[Seq[String]],
        relatedMatch: Option[Seq[String]],
        schemePrefLabel: Option[String],
        scopeNote: Option[String],
        seeAlso: Option[String],
        topConceptOf: Option[Seq[String]],
        types: Option[Seq[String]],
        vocabularyPrefLabel: Option[String]
        )

      ) => RelationType(
      altLabel: Option[Seq[String]],
      broader: Option[Seq[String]],
      broaderMatch: Option[Seq[String]],
      broaderTransitive: Option[Seq[String]],
      broaderTransitivePrefLabels: Option[Seq[String]],
      changeNote: Option[Seq[String]],
      closeMatch: Option[Seq[String]],
      collection: Option[Seq[String]],
      comment: Option[Seq[String]],
      definition: Option[String],
      entityId: Option[String],
      exactMatch: Option[Seq[String]],
      example: Option[String],
      historyNote: Option[String],
      inScheme: Option[Seq[String]],
      narrowerMatch: Option[Seq[String]],
      notation: Option[String],
      note: Option[Seq[String]],
      percoLabel: Option[String],
      prefLabel: Option[Seq[String]],
      related: Option[Seq[String]],
      relatedMatch: Option[Seq[String]],
      schemePrefLabel: Option[String],
      scopeNote: Option[String],
      seeAlso: Option[String],
      topConceptOf: Option[Seq[String]],
      types: Option[Seq[String]],
      vocabularyPrefLabel: Option[String]
    )
  }

  implicit val read: Reads[RelationType] = (read1 and read2) {
    f
  }

  implicit val writes: Writes[RelationType] = new Writes[RelationType] {
    override def writes(o: RelationType): JsValue = Json.obj(
      "altLabel" -> o.altLabel,
      "broader" -> o.broader,
      "broaderMatch" -> o.broaderMatch,
      "broaderTransitive" -> o.broaderTransitive,
      "broaderTransitivePrefLabels" -> o.broaderTransitivePrefLabels,
      "changeNote" -> o.changeNote,
      "closeMatch" -> o.closeMatch,
      "collection" -> o.collection,
      "comment" -> o.comment,
      "definition" -> o.definition,
      "entityId" -> o.entityId,
      "exactMatch" -> o.exactMatch,
      "example" -> o.example,
      "historyNote" -> o.historyNote,
      "inScheme" -> o.inScheme,
      "narrowerMatch" -> o.narrowerMatch,
      "notation" -> o.notation,
      "note" -> o.note,
      "percoLabel" -> o.percoLabel,
      "prefLabel" -> o.prefLabel,
      "related" -> o.related,
      "relatedMatch" -> o.relatedMatch,
      "schemePrefLabel" -> o.schemePrefLabel,
      "scopeNote" -> o.scopeNote,
      "seeAlso" -> o.seeAlso,
      "topConceptOf" -> o.topConceptOf,
      "types" -> o.types,
      "vocabularyPrefLabel" -> o.vocabularyPrefLabel

    )
  }

  implicit val ec: ExecutionContext = ExecutionContext.global

/*
   implicit object RelationTypeId extends IdentifiableNode[EsContext, RelationType] {
    def id(ctx: Context[EsContext, RelationType]): String = ctx.value.id
  }
 */


  val RelationTypeType: ObjectType[Unit, RelationType] =
    deriveObjectType[Unit, RelationType](
      ObjectTypeName("RelationType"),
      ObjectTypeDescription("Relation Type between Materials"),
      DocumentField("altLabel", "Alternative labels for the concept."),
      DocumentField("broader", "Concepts that are broader in meaning than the current concept."),
      DocumentField("broaderMatch", "Concepts that are broader in meaning than the current concept, as a direct mapping."),
      DocumentField("broaderTransitive", "Concepts that are transitively broader in meaning than the current concept."),
      DocumentField("broaderTransitivePrefLabels", "Preferred labels for concepts that are transitively broader in meaning than the current concept."),
      DocumentField("changeNote", "Notes about changes to the concept over time."),
      DocumentField("closeMatch", "Concepts that are closely related to the current concept."),
      DocumentField("collection", "Collections that include the current concept."),
      DocumentField("comment", "Comments related to the concept."),
      DocumentField("definition", "Definition of the concept."),
      DocumentField("entityId", "The ID of the concept."),
      DocumentField("exactMatch", "Concepts that exactly match the current concept."),
      DocumentField("example", "An example demonstrating the concept."),
      DocumentField("historyNote", "Notes about the historical context of the concept."),
      DocumentField("inScheme", "Schemes to which the concept belongs."),
      DocumentField("narrowerMatch", "Concepts that are narrower in meaning than the current concept, as a direct mapping."),
      DocumentField("notation", "A notation representing the concept."),
      DocumentField("note", "Additional notes about the concept."),
      DocumentField("percoLabel", "Label used for percolation."),
      DocumentField("prefLabel", "Preferred labels for the concept."),
      DocumentField("related", "Concepts related to the current concept."),
      DocumentField("relatedMatch", "Concepts related to the current concept, as a direct mapping."),
      DocumentField("schemePrefLabel", "Preferred label for the scheme."),
      DocumentField("scopeNote", "Notes about the scope of the concept."),
      DocumentField("seeAlso", "Related resources or references."),
      DocumentField("topConceptOf", "Schemes for which the concept is a top concept."),
      DocumentField("types", "Types associated with the concept."),
      DocumentField("vocabularyPrefLabel", "Preferred label for the vocabulary.")
      /*
      ReplaceField(
        fieldName = "isPartOf",
        field = Field(
          name = "isPartOf",
          fieldType = materialConnection,
          description = Some("References of materials that this concept is part of."),
          arguments = Connection.Args.All,
          resolve = ctx => {
            val uris = ctx.value.isPartOf.getOrElse(Nil)
            DeferredValue(FetchersResolver.materialFetcher.deferSeq(uris)).map { fetched =>
              Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
            }
          }
        )
      ),
      ReplaceField(
        fieldName = "isComposedOf",
        field = Field(
          name = "isComposedOf",
          fieldType = materialConnection,
          description = Some("References of materials that compose this concept."),
          arguments = Connection.Args.All,
          resolve = ctx => {
            val uris = ctx.value.isComposedOf.getOrElse(Nil)
            DeferredValue(FetchersResolver.materialFetcher.deferSeq(uris)).map { fetched =>
              Connection.connectionFromSeq(fetched, ConnectionArgs(ctx))
            }
          }
        )
      )
       */
    )
}