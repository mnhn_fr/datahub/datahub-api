package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.{EsBooleanQueryBuilder, Searchable}
import com.mnemotix.synaptix.api.graphql.model.GraphqlQueryArgument
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.Document
import fr.mnhn.datahub.api.utils.RDFUtils

import scala.concurrent.{ExecutionContext, Future}

class SearchableDocument(implicit val ec: ExecutionContext) extends Searchable[Document]{
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}collection-item")
  override val searchable: Option[String] = None
  override val searchables: Seq[String] = Seq.empty[String]
  override val fieldSortVal: String = "entityId"

  val rdfType: String = s"${RDFUtils.nameSpaceMap.get("mnhn").get}Document"

  def findSpecificCollectionItem(uris: Option[List[String]]): Future[Seq[Document]] = {
    val arguments = GraphqlQueryArgument(filters = Some(s"$rdfType"))
    val boolQry = EsBooleanQueryBuilder.build(graphQLQueryArgument = arguments, entitiesID = uris)
    if (!IndexClient.connect()) IndexClient.init()
    toObject(boolQry)
  }
}


/*
class SearchableCollectionItem(implicit val ec: ExecutionContext) extends Searchable[CollectionItemEs] {
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}mnhn-collection-item")
  override val searchable: Option[String] = None
  override val searchables: Seq[String] = Seq.empty[String]
  override val fieldSortVal: String = "entityId"

  def findSpecificCollectionItem(uris: Option[List[String]], rdfType: String): Future[Seq[CollectionItemEs]] = {
    val arguments = GraphqlQueryArgument(filters = Some(s"$rdfType"))
    val boolQry = EsBooleanQueryBuilder.build(graphQLQueryArgument = arguments, entitiesID = uris)
    if (!IndexClient.connect()) IndexClient.init()
    toObject(boolQry)
  }
}
 */