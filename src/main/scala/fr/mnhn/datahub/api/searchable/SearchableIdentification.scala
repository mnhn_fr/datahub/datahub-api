package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.Searchable
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.{Identification}

import scala.concurrent.ExecutionContext

class SearchableIdentification(implicit val ec: ExecutionContext) extends Searchable[Identification] {
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}identification")
  override val searchable: Option[String] = Some("identificationID")
  override val searchables: Seq[String] = Seq("identificationID")
  override val fieldSortVal: String = "entityId"

}