package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.Searchable
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.{ArchiveItem, Taxref}

import scala.concurrent.ExecutionContext

class SearchableTaxref(implicit val ec: ExecutionContext) extends Searchable[Taxref] {
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}concept")
  override val searchable: Option[String] = Some("prefLabel")
  override val searchables: Seq[String] = Seq("prefLabel")
  override val fieldSortVal: String = "entityId"
}