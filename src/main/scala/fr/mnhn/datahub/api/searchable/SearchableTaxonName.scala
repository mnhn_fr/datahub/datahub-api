package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.Searchable
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.{TaxonName, Taxref}

import scala.concurrent.ExecutionContext

class SearchableTaxonName(implicit val ec: ExecutionContext) extends Searchable[TaxonName] {
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}concept")
  override val searchable: Option[String] = Some("scientificName")
  override val searchables: Seq[String] = Seq("scientificName",  "preflabel")
  override val fieldSortVal: String = "entityId"
}
