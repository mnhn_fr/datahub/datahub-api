package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.Searchable
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.Material

import scala.concurrent.ExecutionContext

class SearchableMaterial(implicit val ec: ExecutionContext) extends Searchable[Material]{
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}material")
  override val searchables: Seq[String] = Seq("materialEntityID", "entryNumber")
  override val searchable: Option[String] = Some("materialEntityID")
  override val fieldSortVal: String = "entityId"
}
