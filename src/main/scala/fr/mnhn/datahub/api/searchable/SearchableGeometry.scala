package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.Searchable
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.{Geolocation, Geometry}

import scala.concurrent.ExecutionContext

class SearchableGeometry(implicit val ec: ExecutionContext) extends Searchable[Geometry] {
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}geometry")
  override val searchable: Option[String] = Some("locationID")
  override val searchables: Seq[String] = Seq("locationID")
  override val fieldSortVal: String = "entityId"
}