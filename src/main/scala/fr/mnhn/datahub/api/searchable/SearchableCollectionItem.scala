package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.{EsBooleanQueryBuilder, Searchable}
import com.mnemotix.synaptix.api.graphql.model.GraphqlQueryArgument
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.CollectionItemEs

import scala.concurrent.{ExecutionContext, Future}

class SearchableCollectionItem(implicit val ec: ExecutionContext) extends Searchable[CollectionItemEs] {
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}collection-item")
  override val searchable: Option[String] = None
  override val searchables: Seq[String] = Seq.empty[String]
  override val fieldSortVal: String = "entityId"

  def findSpecificCollectionItem(uris: Option[List[String]], rdfType: String): Future[Seq[CollectionItemEs]] = {
    val arguments = GraphqlQueryArgument(filters = Some(s"$rdfType"))
    val boolQry = EsBooleanQueryBuilder.build(graphQLQueryArgument = arguments, entitiesID = uris)
    if (!IndexClient.connect()) IndexClient.init()
    toObject(boolQry)
  }
}