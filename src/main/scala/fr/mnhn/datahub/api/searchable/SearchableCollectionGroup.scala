package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.Searchable
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.{CollectionEvent, CollectionGroup}

import scala.concurrent.ExecutionContext

class SearchableCollectionGroup(implicit val ec: ExecutionContext) extends Searchable[CollectionGroup]{
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}collection-group")
  override val searchables: Seq[String] = Seq("label")
  override val searchable: Option[String] = Some("label")
  override val fieldSortVal: String = "entityId"
}