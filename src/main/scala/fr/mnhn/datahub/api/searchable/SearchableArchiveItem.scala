package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.Searchable
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.{ArchiveItem, Collection}

import scala.concurrent.ExecutionContext

class SearchableArchiveItem(implicit val ec: ExecutionContext) extends Searchable[ArchiveItem] {
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}mnhn-archive-item")
  override val searchable: Option[String] = Some("title")
  override val searchables: Seq[String] = Seq("title")
  override val fieldSortVal: String = "entityId"
}