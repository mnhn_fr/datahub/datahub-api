package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.{EsBooleanQueryBuilder, Searchable}
import com.mnemotix.synaptix.api.graphql.model.GraphqlQueryArgument
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.{Collection, CollectionItemEs}

import scala.concurrent.{ExecutionContext, Future}

class SearchableCollection(implicit val ec: ExecutionContext) extends Searchable[Collection] {
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}mnhn-collection")
  override val searchable: Option[String] = Some("collectionCode")
  override val searchables: Seq[String] = Seq("collectionCode")
  override val fieldSortVal: String = "entityId"

}