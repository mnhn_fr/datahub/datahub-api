package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.{EsBooleanQueryBuilder, Searchable}
import com.mnemotix.synaptix.api.graphql.model.GraphqlQueryArgument
import com.mnemotix.synaptix.index.IndexClient
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.{CollectionItemEs, ConceptRequestable}

import scala.concurrent.{ExecutionContext, Future}

class SearchableConcept(implicit val ec: ExecutionContext) extends Searchable[ConceptRequestable] {
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}concept")
  override val searchable: Option[String] = Some("prefLabel")
  override val searchables: Seq[String] = Seq("prefLabel")
  override val fieldSortVal: String = "entityId"
}
