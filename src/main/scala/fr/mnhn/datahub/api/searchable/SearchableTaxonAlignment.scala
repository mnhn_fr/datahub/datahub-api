package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.Searchable
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.{ArchiveItem, TaxonAlignment}

import scala.concurrent.ExecutionContext

class SearchableTaxonAlignment(implicit val ec: ExecutionContext) extends Searchable[TaxonAlignment] {
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}taxon-alignment")
  override val searchable: Option[String] = None
  override val searchables: Seq[String] = Seq.empty[String]
  override val fieldSortVal: String = "entityId"
}