package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.Searchable
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.RelationType

import scala.concurrent.ExecutionContext

class SearchableRelationType(implicit val ec: ExecutionContext) extends Searchable[RelationType]{
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}concept")
  override val searchables: Seq[String] = Seq("prefLabel")
  override val searchable: Option[String] = Some("prefLabel")
  override val fieldSortVal: String = "entityId"
}