package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.Searchable
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.Taxon

import scala.concurrent.ExecutionContext

class SearchableTaxon(implicit val ec: ExecutionContext) extends Searchable[Taxon]{
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}taxon")
  override val searchables: Seq[String] = Seq("scientificNameAuthorship", "scientificName", "scientificNameWOAuthorship", "taxonID")
  override val searchable: Option[String] = Some("scientificName")
  override val fieldSortVal: String = "entityId"
}