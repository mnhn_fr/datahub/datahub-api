package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.Searchable
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.{CollectionEvent, Material}

import scala.concurrent.ExecutionContext

class SearchableCollectionEvent(implicit val ec: ExecutionContext) extends Searchable[CollectionEvent]{
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}collection-event")
  override val searchables: Seq[String] = Seq("eventID")
  override val searchable: Option[String] = Some("eventID")
  override val fieldSortVal: String = "entityId"
}