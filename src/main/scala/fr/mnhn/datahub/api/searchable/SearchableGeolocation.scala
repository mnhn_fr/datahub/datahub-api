package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.Searchable
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.{Geolocation, Identification}

import scala.concurrent.ExecutionContext

class SearchableGeolocation(implicit val ec: ExecutionContext) extends Searchable[Geolocation] {
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}geolocation")
  override val searchable: Option[String] = Some("country")
  override val searchables: Seq[String] = Seq("country")
  override val fieldSortVal: String = "entityId"
}