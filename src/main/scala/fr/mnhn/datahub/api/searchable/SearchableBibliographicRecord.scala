package fr.mnhn.datahub.api.searchable

import com.mnemotix.synaptix.api.graphql.Searchable
import com.mnemotix.synaptix.index.elasticsearch.ESConfiguration
import fr.mnhn.datahub.api.model.{ArchiveItem, BibliographicRecord}

import scala.concurrent.ExecutionContext

class SearchableBibliographicRecord(implicit val ec: ExecutionContext) extends Searchable[BibliographicRecord] {
  override val INDEX_NAMES: Seq[String] = Seq(s"${ESConfiguration.prefix.getOrElse("")}mnhn-bibliographic-record")
  override val searchable: Option[String] = Some("title")
  override val searchables: Seq[String] = Seq("title")
  override val fieldSortVal: String = "entityId"
}