/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api

import com.mnemotix.synaptix.api.graphql.model.GraphqlQueryArgument.{filtersArg, qsArg, sortingsArg, toGraphqlQueryArgument}
import com.typesafe.scalalogging.LazyLogging
import fr.mnhn.datahub.api.model.ArchiveItem.{ArchiveItemType, archiveItemConnection, _}
import fr.mnhn.datahub.api.model.BibliographicRecord.{BibliographicRecordType, bibliographicRecordConnection, _}
import fr.mnhn.datahub.api.model.Collection.{CollectionType, collectionConnection, _}
import fr.mnhn.datahub.api.model.CollectionEvent.{CollectionEventType, collectionEventConnection, _}
import fr.mnhn.datahub.api.model.CollectionGroup.CollectionGroupType
import fr.mnhn.datahub.api.model.ConceptRequestable.ConceptEsType
import fr.mnhn.datahub.api.model.Document.{DocumentType, documentConnection, _}
import fr.mnhn.datahub.api.model.Geolocation.{GeolocationType, geolocationConnection, _}
import fr.mnhn.datahub.api.model.Geometry.GeometryType
import fr.mnhn.datahub.api.model.Identification.{IdentificationType, identificationConnection, _}
import fr.mnhn.datahub.api.model.Material.{MaterialType, materialConnection, _}
import fr.mnhn.datahub.api.model.MaterialRelationship.MaterialRelationshipType
import fr.mnhn.datahub.api.model.Person.{PersonType, personConnection, _}
import fr.mnhn.datahub.api.model.RelationType.RelationTypeType
import fr.mnhn.datahub.api.model.Taxon.{TaxonType, taxonConnection, _}
import fr.mnhn.datahub.api.model.TaxonAlignment.TaxonAlignmentType
import fr.mnhn.datahub.api.model.TaxonName.TaxonNameType
import fr.mnhn.datahub.api.model.Taxref.TaxrefType
import fr.mnhn.datahub.api.model.{EsContext}
import sangria.schema.{Field, ObjectType, Schema, fields, _}
import sangria.relay._
import scala.concurrent.ExecutionContext

object DatahubSchema extends LazyLogging  {

  implicit val ec: ExecutionContext = ExecutionContext.global

  val NodeDefinition = Node.definition(
      resolve = (id: GlobalId, ctx: Context[EsContext, Unit]) ⇒ {
        if (id.typeName == "ArchiveItemType") {
          ctx.ctx.elastic.searchableArchiveItem.find(s"https://www.data.mnhn.fr/data/archive-item-type/${id.id}")
        }
        else if (id.typeName == "BibliographicRecord") {
          ctx.ctx.elastic.searchableBibliographicRecord.find(s"https://www.data.mnhn.fr/data/bibliographic-record/${id.id}")
        }
        else if (id.typeName == "CollectionEvent") {
          ctx.ctx.elastic.searchableCollectionEvent.find((s"https://www.data.mnhn.fr/data/collection-event/${id.id}"))
        }
        else if (id.typeName == "CollectionGroup") {
          ctx.ctx.elastic.searchableCollectionGroup.find(s"https://www.data.mnhn.fr/data/collection-group/${id.id}")
        }
        else if (id.typeName == "Collection") {
          ctx.ctx.elastic.searchableCollection.find(s"https://www.data.mnhn.fr/data/collection/${id.id}")
        }
        else if (id.typeName == "Document") {
          ctx.ctx.elastic.searchableDocument.find(s"https://www.data.mnhn.fr/data/document/${id.id}")
        }
        else if (id.typeName == "Geolocation") {
          ctx.ctx.elastic.searchableGeolocation.find(s"https://www.data.mnhn.fr/data/geolocation/${id.id}")
        }
        else if (id.typeName == "Geometry") {
          ctx.ctx.elastic.searchableGeometry.find(s"https://www.datahub.mnhn.fr/data/geometry/${id.id}")
        }
        else if (id.typeName == "Material") {
          ctx.ctx.elastic.searchableMaterial.find(s"https://www.datahub.mnhn.fr/data/material/${id.id}")
        }
        else if (id.typeName == "Person") {
          ctx.ctx.elastic.searchablePerson.find(s"https://www.datahub.mnhn.fr/data/person/${id.id}")
        }
        else if (id.typeName == "Taxon") {
          ctx.ctx.elastic.searchableTaxon.find(s"https://www.data.mnhn.fr/data/taxon/${id.id}")
        }
        else if (id.typeName == "TaxonName") {
          ctx.ctx.elastic.searchableTaxonName.find(id.id)
        }

        else if (id.typeName == "Taxref") {
          ctx.ctx.elastic.searchableTaxref.find(id.id)
        }
        else None
      }, Node.possibleNodeTypes[EsContext, Node](
      ArchiveItemType, BibliographicRecordType, CollectionEventType,
      CollectionGroupType, CollectionType, DocumentType, GeolocationType, GeometryType, MaterialType, PersonType, TaxonType,
      TaxonNameType, TaxrefType
    )
  )

  val nodeInterface = NodeDefinition.interface
  val nodeField = NodeDefinition.nodeField
  val nodeFields = NodeDefinition.nodeFields


  val QueryType = ObjectType(
    "Query",
    fields[EsContext, Unit] (
      nodeField,
      nodeFields,
      Field("person",
        personConnection,
        arguments =  qsArg  :: filtersArg :: sortingsArg :: Connection.Args.All,
        resolve = ctx ⇒ {
          val qryArg = ctx.toGraphqlQueryArgument()
          val persons = ctx.ctx.elastic.searchablePerson.search(Seq.empty[String], qryArg, None)
          Connection.connectionFromFutureSeq(persons, ConnectionArgs(ctx))
        }
      ),
      Field("collection",
        collectionConnection,
        arguments = qsArg  :: filtersArg :: sortingsArg :: Connection.Args.All,
        resolve = ctx ⇒ {
          val qryArg = ctx.toGraphqlQueryArgument()
          val collections = ctx.ctx.elastic.searchableCollection.search(Seq.empty[String], qryArg, None)
          Connection.connectionFromFutureSeq(collections, ConnectionArgs(ctx))
        }
      ),
      Field("archiveItem",
        archiveItemConnection,
        arguments = qsArg  :: filtersArg :: sortingsArg :: Connection.Args.All,
        resolve = ctx ⇒ {
          val qryArg = ctx.toGraphqlQueryArgument()
          val archiveItems = ctx.ctx.elastic.searchableArchiveItem.search(Seq.empty[String], qryArg, None)
          Connection.connectionFromFutureSeq(archiveItems, ConnectionArgs(ctx))
        }
      ),
      Field("document",
        documentConnection,
        arguments = qsArg  :: filtersArg :: sortingsArg :: Connection.Args.All,
        resolve = ctx ⇒ {
          val qryArg = ctx.toGraphqlQueryArgument()
          val documents = ctx.ctx.elastic.searchableDocument.search(Seq.empty[String], qryArg, None)
          Connection.connectionFromFutureSeq(documents, ConnectionArgs(ctx))
        }
      ),
      Field("bibliographicRecord",
        bibliographicRecordConnection,
        arguments = qsArg  :: filtersArg :: sortingsArg :: Connection.Args.All,
        resolve = ctx ⇒ {
          val qryArg = ctx.toGraphqlQueryArgument()
          val bibliographicRecords = ctx.ctx.elastic.searchableBibliographicRecord.search(Seq.empty[String], qryArg, None)
          Connection.connectionFromFutureSeq(bibliographicRecords, ConnectionArgs(ctx))
        }
      ),
      Field("taxon",
        taxonConnection,
        arguments = qsArg  :: filtersArg :: sortingsArg :: Connection.Args.All,
        resolve = ctx ⇒ {
          val qryArg = ctx.toGraphqlQueryArgument()
          val taxons = ctx.ctx.elastic.searchableTaxon.search(Seq.empty[String], qryArg, None)
          Connection.connectionFromFutureSeq(taxons, ConnectionArgs(ctx))
        }
      ),
      Field("geolocation",
        geolocationConnection,
        arguments = qsArg  :: filtersArg :: sortingsArg :: Connection.Args.All,
        resolve = ctx ⇒ {
          val qryArg = ctx.toGraphqlQueryArgument()
          val geolocations = ctx.ctx.elastic.searchableGeolocation.search(Seq.empty[String], qryArg, None)
          Connection.connectionFromFutureSeq(geolocations, ConnectionArgs(ctx))
        }
      ),
      Field("identification",
        identificationConnection,
        arguments = qsArg  :: filtersArg :: sortingsArg :: Connection.Args.All,
        resolve = ctx ⇒ {
          val qryArg = ctx.toGraphqlQueryArgument()
          val identifications = ctx.ctx.elastic.searchableIdentification.search(Seq.empty[String], qryArg, None)
          Connection.connectionFromFutureSeq(identifications, ConnectionArgs(ctx))
        },
      ),
      Field("collectionEvent",
        collectionEventConnection,
        arguments = qsArg  :: filtersArg :: sortingsArg :: Connection.Args.All,
        resolve = ctx ⇒ {
          val qryArg = ctx.toGraphqlQueryArgument()
          val collectionEvent = ctx.ctx.elastic.searchableCollectionEvent.search(Seq.empty[String], qryArg, None)
          Connection.connectionFromFutureSeq(collectionEvent, ConnectionArgs(ctx))
        }
      ),
      Field("material",
        materialConnection,
        arguments = qsArg  :: filtersArg :: sortingsArg :: Connection.Args.All,
        resolve = ctx ⇒ {
          val qryArg = ctx.toGraphqlQueryArgument()
          val materials = ctx.ctx.elastic.searchableMaterial.search(Seq.empty[String], qryArg, None)
          Connection.connectionFromFutureSeq(materials, ConnectionArgs(ctx))
        }
      )
    )
  )

  val SchemaDefinition: Schema[EsContext, Unit] = Schema(
    query = QueryType,
    mutation = None,
    additionalTypes = ArchiveItemType :: BibliographicRecordType :: CollectionEventType ::
      CollectionGroupType :: CollectionType :: ConceptEsType ::
      DocumentType :: GeometryType :: GeolocationType ::
      IdentificationType :: MaterialRelationshipType :: MaterialType ::
      PersonType :: RelationTypeType :: TaxonAlignmentType ::
      TaxonNameType :: TaxonType :: TaxrefType :: Nil
  )
}
