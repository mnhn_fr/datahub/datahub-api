/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api.utils

import java.util.Base64

object GraphQLIdUtils {

  /**
   *
   * @param id the ID given by graphQL
   * @param typeName the typeName i.e "Person", "Partenaire"
   * @return an encoded combination of the typeName:ID in Base64
   */

  def encodeId(id: String, typeName: String) = {
    Base64.getEncoder.encodeToString((s"$typeName:$id").getBytes)
  }

  def decodeId(encodedId: String) = {
    val decodedBytes = Base64.getDecoder.decode(encodedId)
    val decodedId = new String(decodedBytes)
    val Array(typeName, id) = decodedId.split(":")
    id
  }
}