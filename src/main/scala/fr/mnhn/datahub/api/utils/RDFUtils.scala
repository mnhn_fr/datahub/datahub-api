/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api.utils

object RDFUtils {

  /**
   * map of name space
   */

  val nameSpaceMap: Map[String, String] = Map(
    "bibo" -> "http://purl.org/ontology/bibo/",
    "bio" -> "http://purl.org/vocab/bio/0.1/",
    "dc" -> "http://purl.org/dc/elements/1.1/",
    "dcat" -> "http://www.w3.org/ns/dcat#",
    "dcmi" -> "http://purl.org/dc/dcmitype/",
    "dcterms" -> "http://purl.org/dc/terms/",
    "dbpedia" -> "http://dbpedia.org/ontology/",
    "dwc" -> "http://rs.tdwg.org/dwc/terms/",
    "dwciri" -> "http://rs.tdwg.org/dwc/iri/",
    "foaf" -> "http://xmlns.com/foaf/0.1/",
    "fnml" -> "http://semweb.mmlab.be/ns/fnml#",
    "fno" -> "https://w3id.org/function/ontology#",
    "grel" -> "http://users.ugent.be/~bjdmeest/function/grel.ttl#",
    "geo" -> "http://www.w3.org/2003/01/geo/wgs84_pos#",
    "geosparql" -> "http://www.opengis.net/ont/geosparql/",
    "hctl" -> "https://www.w3.org/2019/wot/hypermedia#",
    "htv" -> "http://www.w3.org/2011/http#",
    "isbd" -> "http://iflastandards.info/ns/isbd/elements/",
    "isni" -> "http://isni.org/ontology/",
    "marcrel" -> "http://id.loc.gov/vocabulary/relators/",
    "mnhn" -> "https://www.data.mnhn.fr/ontology/",
    "mnhnd" -> "https://www.data.mnhn.fr/data/",
    "mnx" -> "http://ns.mnemotix.com/ontologies/2019/8/generic-model/",
    "owl" -> "http://www.w3.org/2002/07/owl#",
    "prov" -> "http://www.w3.org/ns/prov#",
    "rdf" -> "http://www.w3.org/1999/02/22-rdf-syntax-ns#",
    "rdfs" -> "http://www.w3.org/2000/01/rdf-schema#",
    "rdam" -> "http://rdaregistry.info/Elements/m/",
    "rdaelements" -> "http://rdvocab.info/Elements/",
    "rdau" -> "http://rdaregistry.info/Elements/u/",
    "ro" -> "http://purl.obolibrary.org/obo/",
    "rr" -> "http://www.w3.org/ns/r2rml#",
    "schema" -> "http://schema.org/",
    "skos" -> "http://www.w3.org/2004/02/skos/core#",
    "skosxl" -> "http://www.w3.org/2008/05/skos-xl#",
    "taxref" -> "http://taxref.mnhn.fr/lod/",
    "taxrefbgs" -> "http://taxref.mnhn.fr/lod/bioGeoStatus/",
    "taxrefhab" -> "http://taxref.mnhn.fr/lod/habitat/",
    "taxrefloc" -> "http://taxref.mnhn.fr/lod/loc/",
    "taxrefprop" -> "http://taxref.mnhn.fr/lod/property/",
    "taxrefrk" -> "http://taxref.mnhn.fr/lod/taxrank/",
    "taxrefstatus" -> "http://taxref.mnhn.fr/lod/status/",
    "td" -> "https://www.w3.org/2019/wot/td#",
    "tdwgutility" -> "http://rs.tdwg.org/dwc/terms/attributes/",
    "tn" -> "http://rs.tdwg.org/ontology/voc/TaxonName#",
    "void" -> "http://rdfs.org/ns/void#",
    "wd" -> "http://www.wikidata.org/entity/",
    "wdt" -> "http://www.wikidata.org/prop/direct/",
    "xsd" -> "http://www.w3.org/2001/XMLSchema#"
  )
}
