/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api.utils

import play.api.libs.json.{JsonValidationError, Reads}

import scala.collection.immutable
import scala.util.{Success, Try}

/**
 * Object function that convert a String to a Seq[String] when reading to json
 */

object MnxJsonUtils {
  val readSeqFromString: Reads[Seq[String]] = {
    implicitly[Reads[String]]
      .map(x => Try(Seq(x)))
      .collect(JsonValidationError(Seq("Parsing error"))) {
        case seq: immutable.Seq[_] => Seq()
        case Success(a) => a
      }
  }
  val readSeq: Reads[Seq[String]] = implicitly[Reads[Seq[String]]].orElse(readSeqFromString)
}
