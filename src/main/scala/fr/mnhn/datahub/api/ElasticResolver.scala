/**
 * Copyright (C) 2013-2024 MNEMOTIX <http://www.mnemotix.com/> and/or its affiliates
 * and other contributors as indicated by the @author tags.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package fr.mnhn.datahub.api

import akka.actor.ActorSystem
import com.typesafe.scalalogging.LazyLogging
import fr.mnhn.datahub.api.searchable.{SearchableArchiveItem, SearchableBibliographicRecord, SearchableCollection, SearchableCollectionEvent, SearchableCollectionGroup, SearchableCollectionItem, SearchableConcept, SearchableDocument, SearchableGeolocation, SearchableGeometry, SearchableIdentification, SearchableMaterial, SearchablePerson, SearchableRelationType, SearchableTaxon, SearchableTaxonAlignment, SearchableTaxonName, SearchableTaxref}

import scala.concurrent.ExecutionContext

class ElasticResolver() extends DatahubApi with LazyLogging {
  override implicit val system: ActorSystem = ActorSystem("ElasticResolver")
  override implicit val executionContext: ExecutionContext = system.dispatcher

  lazy val searchableCollectionItem = new SearchableCollectionItem()
  lazy val searchableDocument = new SearchableDocument()
  lazy val searchablePerson = new SearchablePerson()
  lazy val searchableMaterial = new SearchableMaterial()
  lazy val searchableTaxon = new SearchableTaxon()
  lazy val searchableCollection = new SearchableCollection()
  lazy val searchableIdentification = new SearchableIdentification()
  lazy val searchableCollectionEvent = new SearchableCollectionEvent()
  lazy val searchableCollectionGroup = new SearchableCollectionGroup()
  lazy val searchableGeolocation = new SearchableGeolocation()
  lazy val searchableGeometry = new SearchableGeometry()
  lazy val searchableConcept = new SearchableConcept()
  lazy val searchableArchiveItem = new SearchableArchiveItem()
  lazy val searchableTaxref = new SearchableTaxref()
  lazy val searchableTaxonName = new SearchableTaxonName()
  lazy val searchableRelationType = new SearchableRelationType()
  lazy val searchableBibliographicRecord = new SearchableBibliographicRecord()
  lazy val searchableTaxonAlignment = new SearchableTaxonAlignment()
}
