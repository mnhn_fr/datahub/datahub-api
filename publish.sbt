ThisBuild / scalaVersion := Version.scalaVersion
ThisBuild / organization := "com.mnemotix"
ThisBuild / organizationName := "MNEMOTIX SCIC"
ThisBuild / licenses := List("Apache 2" -> new URI("http://www.apache.org/licenses/LICENSE-2.0.txt").toURL)

Global / onChangedBuildSource := ReloadOnSourceChanges

ThisBuild / developers := List(
  Developer(
    id = "ndelaforge",
    name = "Nicolas Delaforge",
    email = "nicolas.delaforge@mnemotix.com",
    url = url("http://www.mnemotix.com")
  ),
  Developer(
    id = "prlherisson",
    name = "Pierre-René Lherisson",
    email = "pr.lherisson@mnemotix.com",
    url = url("http://www.mnemotix.com")
  )
)

ThisBuild / useCoursier := false
//ThisBuild / onChangedBuildSource := ReloadOnSourceChanges
ThisBuild / credentials += Credentials(Path.userHome / ".sbt" / ".credentials.gitlab")

//val glHost        = "gitlab.com"
//val glGroup       = sys.env.getOrElse("GROUP_ID", 5299945)
//val glGrpRegistry  = s"https://$glHost/api/v4/groups/$glGroup/-/packages/maven"

ThisBuild / resolvers ++= Seq(
  Resolver.mavenLocal,
  Resolver.typesafeRepo("releases"),
  Resolver.sbtPluginRepo("releases"),
  "gitlab-maven" at "https://gitlab.com/api/v4/projects/21727073/packages/maven",
  "gitlab-maven_2" at "https://gitlab.com/api/v4/projects/375779/packages/maven"
)
